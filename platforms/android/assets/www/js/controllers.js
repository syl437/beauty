angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$localStorage,$ionicPopup,$http,$ionicLoading,$q,$rootScope,$translate) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  $scope.$on('$ionicView.enter', function(e) {
  

	$scope.LoginType = 0;
	$scope.BackModalType = 0;
	$scope.IsloggedIn = false;
	

	
	$scope.checkLoggedIn = function(type)
	{
		if (!$localStorage.userid)
		{
			var myPopup = $ionicPopup.show({
			//template: '<input type="text" ng-model="data.myData">',
			//template: '<style>.popup { width:500px; }</style>',
			title: $translate.instant('registerorloginfirstctrl')+':',
			scope: $scope,
			cssClass: 'custom-popup',
			buttons: [

		   {
			text: $translate.instant('membersloginctrl'),
			type: 'button-positive',
			onTap: function(e) { 
				if (type == 2)
				{
					//$scope.openLoginModal();
					window.location ="#/app/login/"+type;
				}
				else
				{
					window.location ="#/app/login/"+type;
				}
			}
		   },
		   {
			text: $translate.instant('newmembersregisterctrl'),
			type: 'button-calm',
			onTap: function(e) { 
			//$scope.openRegisterModal();
			window.location ="#/app/register/"+type;
			 
			}
		   },
			   {
			text: $translate.instant('canceltextctrl'),
			type: 'button-assertive',
			onTap: function(e) {  
			  //alert (1)
			}
		   },
		   ]
		  });
	  
		}
		else
		{
			return true;
		}
	}

	
	$scope.CouponsNav = function(type)
	{
		$scope.LoginType = type;
		window.location ="#/app/coupons";
		
		//if ($scope.checkLoggedIn(type))
		//{
		//}
	}
	
	$scope.myOrders = function(type)
	{
		$scope.LoginType = type;
		if ($scope.checkLoggedIn(type))
		{
			window.location ="#/app/appointments";
		}		
	}


	$scope.openRegisterModal = function()
	{
		  $ionicModal.fromTemplateUrl('templates/register_modal.html', {
			scope: $scope
		  }).then(function(registerModal) {
			$scope.registerModal = registerModal;
			$scope.registerModal.show();
		  });		
	}
		
	
	$scope.openLoginModal = function()
	{
		  $ionicModal.fromTemplateUrl('templates/login_modal.html', {
			scope: $scope
		  }).then(function(loginModal) {
			$scope.loginModal = loginModal;
			$scope.loginModal.show();
		  });		
	}		


	$scope.closeLogin = function()
	{
		$scope.loginModal.hide();
	}
	
	$scope.closeRegister = function()
	{
		$scope.registerModal.hide();
	}

	
	$scope.LogOut = function()
	{
		$scope.IsloggedIn = false;
		$scope.fullName = '';		
		$localStorage.userid = '';
		$localStorage.fbid = '';
		$localStorage.name = '';
		$localStorage.email = '';
		$localStorage.phone = '';
		$localStorage.password = '';
	}
	
	$scope.openPage = function(page)
	{
		//alert (page);
	}

/*
	$rootScope.$watch('pushRedirect', function () 
	{   
		if ($rootScope.pushRedirect)
		{
			window.location ="#/app/ratings/"+$rootScope.pushRedirect;
			$rootScope.pushRedirect = "";
		}

	}, true);
*/
	
	
	if (!$localStorage.userid)
	{
		$scope.IsloggedIn = false;
		$scope.fullName = '';
	}
	else
	{
		$scope.IsloggedIn = true;
		$scope.fullName = $localStorage.name;
	}
	
	$scope.languageSelect = function()
	{
		//$scope.changeLanguage('ru');

		var config = {
			title: $translate.instant('selectlanguage'), 
			items: [
				{ text: "עברית", value: "he" },
				{ text: "English", value: "en" },
				{ text: "русский", value: "ru" },
				{ text: "العبرية", value: "ar" }
			],
			selectedValue: $rootScope.DefaultLanguage,
			doneButtonLabel: $translate.instant('loadingtextctrl'),
			cancelButtonLabel: $translate.instant('loadingtextctrl')
		};

		// Show the picker
		window.plugins.listpicker.showPicker(config, 
			function(item) { 

			$timeout(function() {
			  $scope.changeLanguage(item);
			}, 300);
	
			},
			function() { 
				//alert("You have cancelled");
			}
		);		
	}
		
	$scope.changeLanguage = function(lang)
	{
		if(lang == 'en' || lang == 'ru')
		$rootScope.lanDirction = 'ltr';
		else
		$rootScope.lanDirction = 'rtl';
		
		$rootScope.DefaultLanguage = lang;
		$translate.use(lang);
	}	
	
	
		

	
});	
})



.controller('MainCtrl', function($scope, $stateParams,$rootScope,loadMainJson,$localStorage,$ionicModal,$ionicLoading,$http,$ionicPopup,$q,$cordovaDatePicker,$translate) 
{	


	$scope.host = $rootScope.Host;
	$scope.currentLanguage = $rootScope.DefaultLanguage;


	
	//$rootScope.NavigateMainSearch = '';
	

	

	$rootScope.$watch('DefaultLanguage', function () 
	{   
		$scope.currentLanguage = $rootScope.DefaultLanguage;

	});	
	
	
	
	$rootScope.MainSearchArray = [];
	
	$scope.fields = 
	{
		"search" : ""
	}
	
	loadMainJson.loadMain().then(function(data)
	{
		if (data.length > 0)
		{
			$scope.Catagories = data;
//			console.log("CT : " ,$scope.Catagories);		
		}
	});
	


  
	$scope.doSearch = function()
	{
		if ($scope.fields.search)
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


		
				$ionicLoading.show({
				  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
				});		
		

				send_params = 
				{
					"search" : $scope.fields.search
				}

				
				$http.post($rootScope.Host+'/main_search.php',send_params)
				.success(function(data, status, headers, config)
				{
					$ionicLoading.hide();
				//	$rootScope.MainSearchArray = [];
					//$rootScope.SearchJson = data;
					console.log('search results: ' , data)
					if (data.length > 0)
					{
						$rootScope.MainSearchArray = data;
						console.log("Main : " , $rootScope.MainSearchArray)
						//$rootScope.SearchJson = data;
						$rootScope.NavigateMainSearch = 1;
						window.location = "#/app/listmain/";
						
					}
					else 
					{
							$ionicPopup.alert({
							 title: $translate.instant('nomatchestryagainctrl'),
							 template: '',
							 okText: $translate.instant('oktextctrl'),	
						   });	
					}
					
				})
				.error(function(data, status, headers, config)
				{
					$ionicLoading.hide();
				});			
		}
		else
		{
			$ionicPopup.alert({
				 title: $translate.instant('inputsuppliernamectrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),	
			   });				
		}

	}
     $scope.playMainVideo = function()
	 {
		  $ionicModal.fromTemplateUrl('templates/main_video.html', {
			scope: $scope
		  }).then(function(mainVideoModal) {
			$scope.mainVideoModal = mainVideoModal;
			$scope.mainVideoModal.show();
		  });			 
	 }

	$scope.stopVideo = function()
	{
		var div = document.getElementById('videoWrapper');
		var player = document.getElementById('VideoPlayer');
		div.innerHTML = '';
		div.remove();
		player.src='';
		//player.pause(); 

	}

	

	$scope.closeVideoModal= function()
	{
		$scope.mainVideoModal.hide();
		$scope.stopVideo();
	}		  
	
	$scope.enterPress = function(keyEvent)
	{
		if (keyEvent.which === 13)
		{
			$scope.doSearch();
		}
	}
	
	
$scope.$on("$ionicView.afterLeave", function(event, data){
   
	//$scope.stopVideo();
});

	
})


.controller('SearchCtrl', function($scope, $stateParams,loadMainJson,$rootScope,$state,$ionicLoading,$http,dateFilter,$ionicPopup,$timeout,$translate) {
	
	//$scope.$on('$ionicView.enter', function(e) {
	$scope.ItemId = $stateParams.ItemId;
	$scope.show = 1;
	$scope.ApptSelected = false;
	$scope.ServiceSelected = true;
	
	/*
	$scope.$on('$ionicView.enter', function(e) {
		//$rootScope.NavigateMainSearch = '';
	});	
	*/
	
	$scope.currentLanguage = $rootScope.DefaultLanguage;
	
	$rootScope.$watch('DefaultLanguage', function () 
	{   
		$scope.currentLanguage = $rootScope.DefaultLanguage;

	});	

	
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	
	if(dd<10) {
		dd='0'+dd
	} 

	if(mm<10) {
		mm='0'+mm
	} 

	$scope.today = dd+'/'+mm+'/'+yyyy;
	$scope.subcatSelected = 0;
	
			
	loadMainJson.loadMain().then(function(data){
		
		
		console.log("loadMain:" , data);
		
		if (data.length > 0)
		{
			$scope.Catagories = data;
			$scope.Subcatagories = $scope.Catagories[$scope.ItemId].subcategories;
			$scope.CatagoryIndex = $scope.Catagories[$scope.ItemId].index;
			//alert ($scope.CatagoryIndex)
			//console.log("Sub : " ,$scope.Subcatagories)
			
			
			$scope.search = 
			{
				"area" : "0",
				"subcat" : "0",
				"day" :  $scope.today,//  "28/01/2016","בחר תאריך", //
				"time" : "", // "17:00" //בחר שעה" 
			}	
			
		
			
/*
			$timeout(function() {
			  $scope.search.subcat = $scope.Subcatagories[0].index;
			}, 1000);
*/

		}
	});
	
	$scope.setSelected = function(num)
	{
		console.log(num)
		$scope.subcatSelected = 1;
	}
	
	$scope.pickTime = function()
	{
		var options = {
			date: new Date(),
			mode: 'time',
			androidTheme: 2,
			//minuteInterval: 10,
			is24Hour : true,
			okText : $translate.instant('datepickoktext'),
			cancelText: $translate.instant('datepickcanceltext')
		};



		//datePicker.show(options, $scope.dateonSuccess, $scope.dateonError);		
		
		
		datePicker.show(options, function(date){
		  //alert("date result " + date);  
			$scope.newHour = date.toString().split(" ")[4];
			$scope.splitHour = $scope.newHour.split(":");
			$scope.newTime = $scope.splitHour[0]+":"+$scope.splitHour[1];
			$scope.search.time = $scope.newTime;
			$scope.updateTime();
		
		});
	}
	
	
	$scope.updateTime = function()
	{
		 $timeout(function() {
			$scope.search.time = $scope.newTime;
		}, 100);
	}

	


	
	$scope.pickDate = function()
	{
		var options = {
			date: new Date(),
			mode: 'date',
			androidTheme: 2,
			//minuteInterval: 10,
			titleText  : $translate.instant('search_date'),
			okText : $translate.instant('datepickoktext'),
			cancelText: $translate.instant('datepickcanceltext')
		};



		//datePicker.show(options, $scope.dateonSuccess, $scope.dateonError);		
		
		
		datePicker.show(options, function(date){
			var a = date;
			var b = a.getFullYear();
			var c = a.getMonth();
			(++c < 10)? c = "0" + c : c;
			var d = a.getDate();
			(d < 10)? d = "0" + d : d;
			var fulldate = b + "-" + c + "-" + d; 
			var finaldate = d + "/" + c + "/" + b; 
			
			$scope.newDate = finaldate;
			$scope.search.day = $scope.newDate;
			$scope.updateDate();
		
		});
	}

	
	$scope.updateDate = function()
	{
		 $timeout(function() {
			$scope.search.day = $scope.newDate;
		}, 100);
	}

	
	$scope.dateonSuccess = function (date) {
		$scope.newHour = date.toString().split(" ")[4];
		$scope.splitHour = $scope.newHour.split(":");
		$scope.newTime = $scope.splitHour[0]+":"+$scope.splitHour[1];
		$scope.search.time = $scope.newTime;
	}

	$scope.dateonError = function (error) { // Android only
		//alert('Error: ' + error);
	}
	
		
		/*
		$scope.$watch('search.time', function () 
		{   
			$scope.search.time = $scope.newTime;

		});
		*/

	
	$scope.service = function(type)
	{
		if (type == 1)
		{
			$scope.show = 0;
			$scope.ApptSelected = true;
			$scope.ServiceSelected = false;
		}
		else
		{
			$scope.show = 1;
			$scope.ApptSelected = false;
			$scope.ServiceSelected = true;
		}
	}
	/*

		$rootScope.searchParams = 
	{
	  "area" : "",
	  "type" : "",
	  "catagory" : "",
	  "date" : "",
	  "time" : ""
	}
	*/
	
	$scope.searchBtn = function()
	{

		
		$scope.isHistory = 0;
		
		if ($scope.show == 0)
		{
			var DaysArray = $scope.search.day.split("/");
			var CurrentDay = DaysArray[1] + "/"+ DaysArray[0] +"/"+ DaysArray[2] +"   "+ $scope.search.time ;
			var datum = new Date(CurrentDay).getTime()
			
			if(Math.floor(Date.now() / 1000) > (datum/1000) )
			{
				$scope.isHistory  = 1;

			}
			
			console.log(Math.floor(Date.now() / 1000) + " : "  + (datum/1000))
		}
		
		

		if ($scope.search.area == "0")
		{
			$ionicPopup.alert({
				 title: $translate.instant('selectareactrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),			 
			   });				
		}
		else if ($scope.search.subcat =="0")
		{
			$ionicPopup.alert({
				 title: $translate.instant('selectsubcatctrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),	
			   });			
		}
		
		else if ($scope.search.time == "" && $scope.show == 0)
		{
			$ionicPopup.alert({
			 title: $translate.instant('selecttimectrl'),
			 template: '',
			 okText: $translate.instant('oktextctrl'),	
			});			
		}
		
		else if ($scope.isHistory == 1 && $scope.show == 0)
		{
			$ionicPopup.alert({
			 title: $translate.instant('cantaddtorpassedctrl'),
			 template: '',
			 okText: $translate.instant('oktextctrl'),	
			});			
		}				
		else
		{
			$rootScope.NavigateMainSearch = '';
			$scope.Sapakim = [];
			$rootScope.Sapakim = [];
			$rootScope.SearchJson = [];		
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
			
			$rootScope.searchParams.area = $scope.search.area;
			$rootScope.searchParams.type = $scope.show;
			$rootScope.searchParams.catagory = $scope.search.subcat;
			$rootScope.searchParams.day = $scope.search.day;
			$rootScope.searchParams.time = $scope.search.time;
			//console.log($rootScope.searchParams);
			//alert ($rootScope.searchParams.day);
		
	
		
				$ionicLoading.show({
				  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
				});		
		
				$scope.newdate = dateFilter($scope.search.day, 'dd/MM/yyyy');

				send_params = 
				{
					"catagory" : $scope.CatagoryIndex,
					"area" : $scope.search.area,
					"subcat" : $scope.search.subcat,
					"day" : $scope.newdate,
					"time" : $scope.search.time,
					"type" : $scope.show,
					"queu_len" : 22
				}
				
				//console.log("Today1 : " ,$scope.today + " : " + $scope.newdate)
				
				
				if ($scope.show == 1)
					$scope.Url = $rootScope.Host+'/general_search.php';
				else
					$scope.Url = 'http://tapper.co.il/mami/laravel/public/searchAppointment';
				
				
				console.log("PPP : " , send_params , " : " , $scope.Url )
				$http.post($scope.Url,send_params)
				//$http.post($rootScope.Host+'/'+$scope.Url,send_params)
				.success(function(data, status, headers, config)
				{
					$rootScope.SelectedSubCategory = $scope.search.subcat;
					
					for(i=0;i<$scope.Subcatagories.length;i++)
					{
						if($scope.Subcatagories[i].index == $rootScope.SelectedSubCategory)
						{
							$rootScope.SelectedSubCategoryName = $scope.Subcatagories[i]; // $scope.Subcatagories[i].name;
						}
					}
					
					$ionicLoading.hide();
					$rootScope.SearchJson = data;
					console.log('search results: ' , data)
					
					if ($rootScope.SearchJson != 0)
					{
						$rootScope.selectedCategory = $scope.search.subcat;
						window.location = "#/app/list/"+$scope.ItemId;
					}
					else 
					{
						$ionicPopup.alert({
							 title: $translate.instant('nomatchestryagainctrl'),
							 template: '',
							 okText: $translate.instant('oktextctrl'),	
						   });	
					}
					
				})
				.error(function(data, status, headers, config)
				{
					$ionicLoading.hide();
				});			
		}
		
		


	}
	
	//});	
})


.controller('ListCtrl', function($scope, $stateParams,loadMainJson,$rootScope) 
{
	$scope.TypeSelected = 1;
	$scope.ItemId = $stateParams.ItemId;
	$scope.host = $rootScope.Host;
	$scope.SelectedSubCategory = $rootScope.SelectedSubCategoryName 
	// 1 = service , 0 = appoiment
	$scope.Type = $rootScope.searchParams.type;
	$scope.ShowSearch = 0;
	
	if ($scope.Type =="")
	{
		$scope.Type = 0;
	}
	
	$scope.currentLanguage = $rootScope.DefaultLanguage;
	
	$rootScope.$watch('DefaultLanguage', function () 
	{   
		$scope.currentLanguage = $rootScope.DefaultLanguage;

	});		
	
	
	$scope.changeType = function(Type)
	{
		$scope.TypeSelected = Type;
		$scope.changeOrder();
	}

	$scope.getFloat= function(str)
	{
		str = String(str);
		
		$scope.floatarray = str.split('.');
		$scope.str = parseInt($scope.floatarray[1]);
		if ($scope.str > 49)
			return true;
		else
			return false;
		
	}
	//console.log($rootScope.NavigateMainSearch)
	if ($rootScope.NavigateMainSearch == "")
	{
		$scope.ShowSearch = 1;
		$scope.Sapakim = $rootScope.SearchJson;
	}
	else
	{
		console.log("Tor " ,$rootScope.MainSearchArray )
		$scope.ShowSearch = 0;
		$scope.Sapakim = $rootScope.MainSearchArray;
	}	
	
	$scope.changeOrder = function(item)
	{
		if ($scope.TypeSelected == 1)
		{
			$scope.arrangeBy = "index";
		}
		else if ($scope.TypeSelected == 2)
		{
			$scope.arrangeBy = "-rating";
		}
		else if ($scope.TypeSelected == 3)
		{
			if ($scope.arrangeBy == "price")
			$scope.arrangeBy = "-price";
			else
			$scope.arrangeBy = "price";	
		}			
		//alert ($scope.arrangeBy);
	}
	
	$scope.changeOrder();
	
	//$scope.Sapakim = $rootScope.SearchJson;
	//console.log('Sapakim: ', $scope.Sapakim)
	//alert ($scope.Sapakim);

	if ($rootScope.searchParams.type == 0)
		$scope.showPrice = true;
	else
		$scope.showPrice = true;
	
})

.controller('detailstCtrl', function($scope, $stateParams,loadMainJson,$rootScope,$ionicModal,$ionicPopup,$sce,$http,$localStorage,$state,$q,$ionicLoading,$timeout,$translate) 
{
	//$scope.$on('$ionicView.enter', function(e) 
	//{
  	 //$timeout(function() {
	$scope.TypeSelected = 1;
	$scope.CatagoryIndex = $stateParams.CatIndex;
	$scope.ArrayId = $stateParams.IndexArray;
	$scope.host = $rootScope.Host;
	$scope.Type = $rootScope.searchParams.type;
	$scope.Full = "img/s1.png";	
	$scope.Empty = "img/s2.png";
	$scope.Rate = 0;
	$scope.ItemIndex = $stateParams.ItemIndex;
	$scope.Appoinement = "";
	$scope.SubCut = "";
	$scope.GalleryCount = 0;
	$scope.SelectedSubCategory = $rootScope.SelectedSubCategoryName
	$scope.SubCategory = $rootScope.SelectedSubCategory;
	$scope.ShowOrderButton = true;
	$scope.Supplier = [];
	$scope.Sapak = [];	
	$scope.Gallery = [];
	$scope.showOrderBtn = 0;
	
	//alert ($scope.ItemIndex)
	//alert ($scope.CatagoryIndex)
	//alert ($scope.ItemIndex)
	
	$scope.$on('$ionicView.enter', function(e) {
		//$rootScope.NavigateMainSearch = '';
	});	
		
	$scope.currentLanguage = $rootScope.DefaultLanguage;
	


	
	$scope.RateArray = new Object();
	$scope.RateArray =
	{
		"rate1" : $scope.Empty,
		"rate2" : $scope.Empty,
		"rate3" : $scope.Empty,
		"rate4" : $scope.Empty,
		"rate5" : $scope.Empty,
		"val" : 0
	}


				
	$scope.Comments = [];
	$scope.changeType = function(Type)
	{
		$scope.TypeSelected = Type;
	}

	
	$scope.BackModalType = 0;

	//alert ($scope.CatagoryIndex)
	//alert ($scope.ArrayId)

	//$scope.Sapak = $rootScope.SearchJson[0].suppliers[0];
	//$scope.Gallery = $scope.Sapak.gallery;
	//$scope.Banners = $scope.Sapak.banners;
	
	$scope.getFloat= function(str)
	{
		str = String(str);
		
		$scope.floatarray = str.split('.');
		$scope.str = parseInt($scope.floatarray[1]);
		if ($scope.str > 49)
			return true;
		else
			return false;
		
	}
	
	
	if ($rootScope.NavigateMainSearch =="")
	{  
		if ($scope.Type == 1)
		{
			for(i=0;i< $rootScope.SearchJson.length;i++)
			{
				if ($rootScope.SearchJson[i].index == $scope.ItemIndex)
				{
					$scope.Sapak = $rootScope.SearchJson[i];
				}
			}
			
			//$scope.Sapak = $rootScope.SearchJson[$scope.ArrayId];
			$scope.Gallery = $scope.Sapak.gallery;
			$scope.Banners = $scope.Sapak.banners;
			$scope.Subcat = $scope.Sapak.subcat;
			$rootScope.sapakSubcat = $scope.Subcat;
			$scope.GalleryCount = $scope.Gallery.length;
		}
		else 
		{
			
			for(i=0;i< $rootScope.SearchJson.length;i++)
			{
				if ($rootScope.SearchJson[i].index == $scope.ItemIndex)
				{
					$scope.Supplier = $rootScope.SearchJson[i];
				}
			}
			

			if ($scope.Supplier != "")
			{
				
				$scope.Sapak = $scope.Supplier.suppliers[0];
				console.log("scope.Sapak: " , $scope.Sapak)
				$scope.SupplierData = $scope.Supplier;
				$scope.Gallery = $scope.Supplier.gallery;
				$scope.GalleryCount = $scope.Gallery.length;
				
				$scope.Banners = $scope.Supplier.banners;
				$scope.Appoinement = $scope.Supplier;
			}

			

			
			$scope.DaysWeek = new Array(
			  $translate.instant('daysoftheweek1ctrl'),
			  $translate.instant('daysoftheweek2ctrl'),
			  $translate.instant('daysoftheweek3ctrl'),
			  $translate.instant('daysoftheweek4ctrl'),
			  $translate.instant('daysoftheweek5ctrl'),
			  $translate.instant('daysoftheweek6ctrl'),
			  $translate.instant('daysoftheweek7ctrl')
			);


			
			$scope.reversedate =  $scope.Appoinement.start_date.split("-").reverse().join("/");;
			$scope.splitdate = $scope.reversedate.split("/");
			$scope.formatteddate = $scope.splitdate[2]+'-'+$scope.splitdate[1]+'-'+$scope.splitdate[0];
			$scope.NewDateFormat = new Date($scope.formatteddate);
			
			$scope.DayName = $scope.DaysWeek[$scope.NewDateFormat.getDay()];


		
		
			console.log('appointment : ',$scope.Appoinement.catagory);
			console.log("categories: " , $scope.Appoinement)
			console.log ("Sapak: " , $scope.Sapak)

			$scope.Sapak.catagory = $scope.Appoinement.catagory;

		}


			
	}
	else
	{
			for(i=0;i< $rootScope.MainSearchArray.length;i++)
			{
				console.log("s : "  + $rootScope.MainSearchArray[i].index + " : " + $scope.ItemIndex)
				if ($rootScope.MainSearchArray[i].index == $scope.ItemIndex)
				{
					$scope.Sapak = $rootScope.MainSearchArray[i];
				}
			}

			console.log("Sapak:" , $scope.Sapak)
			
			//$scope.Sapak = $rootScope.MainSearchArray[$scope.ArrayId];
			
			$scope.Gallery = $scope.Sapak.gallery;
			$scope.GalleryCount = $scope.Gallery.length;
			$scope.Banners = $scope.Sapak.banners;
			$scope.Appoinement = $scope.Sapak ;	
			
			$scope.Type = 1;
	}
	

	$scope.SapakDesc = $sce.trustAsHtml($scope.Sapak.desc);
	
	//alert ($scope.SupplierData.price)
	
	//alert ($scope.Sapak.index)
	if ($scope.Sapak.comments)
	$scope.Comments = $scope.Sapak.comments;
	
	if($scope.Appoinement)
	$scope.SubCut = $scope.Appoinement.subcategories;
	
	for(i=0;i<$scope.SubCut.length;i++)
	{
		if($rootScope.SelectedSubCategory == $scope.SubCut[i].subcat_index)
		{
			$scope.Appoinement.len = $scope.SubCut[i].time;
			console.log("Appointment : " , $scope.Appoinement)
		}
	}
	


	

/*  login register functions */


	$scope.OrderAppoint = function(type)
	{
		 if ($scope.checkLoggedIn(type))
		 {
			   var confirmPopup = $ionicPopup.confirm({
				 title: '<p dir="'+$rootScope.lanDirction+'">'+$translate.instant('confirmtorctrl')+'</p>',				   
				 template: '',
				 cancelText: $translate.instant('canceltextctrl'), 
				 okText: $translate.instant('oktextctrl')		 
			   });
				console.log("saveAppointment1")
			   confirmPopup.then(function(res) {
				if(res) 
				{

					$ionicLoading.show({
					  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
					});							  

						
					console.log("saveAppointment2")
					$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
					//$rootScope.SelectedSubCategory 
					
					send_params = 
					{
						"userId":$localStorage.userid,
						"appointmentId":$scope.Appoinement.index,
						"supplier_id":$scope.Sapak.index,
						"start":$scope.Appoinement.start_time,
						"len" :$scope.Appoinement.len,
						"subcat" : $scope.SubCategory,
						"pushid" : $rootScope.pushId
					}
		
					console.log("saveAppointment3 " , send_params)
					//$http.post($rootScope.Host+'/save_Appointment.php',send_params)
					$http.post('http://tapper.co.il/mami/laravel/public/saveAppointment',send_params)
					.success(function(data, status, headers, config)
					{
						$ionicLoading.hide();
						console.log("saveAppointment4")
						$ionicPopup.alert({
							 title: $translate.instant('thankyoutorctrl'),
							 template: '',
							okText: $translate.instant('oktextctrl'),	
						   });
						   
						$scope.ShowOrderButton = false;   

						//$scope.Appoinments = {};
					})
					.error(function(data, status, headers, config)
					{

					});	
				} 
				 else 
				 {
				 }
			   });			 
			 }
				 

	}

	$scope.SearchAppoint = function(type)
	{
		$scope.BackModalType = type;
		 if ($scope.checkLoggedIn(type))
		 {
			 //alert ($scope.ItemIndex);
			 //alert ($scope.ArrayId)
			//alert (111);
			if ($rootScope.NavigateMainSearch =="" )
			{
				//
				$state.go('app.searchappoint',{CatIndex:$scope.CatagoryIndex,IndexArray:$scope.ItemIndex});
			}
			else
			{
				$state.go('app.searchappoint',{CatIndex:$scope.CatagoryIndex,IndexArray:$scope.ArrayId});
			}
			
			 
		 }

	}
	
	$scope.CommentModal = function(type)
	{
		$scope.BackModalType = type;
		
		 if ($scope.checkLoggedIn(type))
		 {
			  $scope.data = {};

			  // An elaborate, custom popup
				$ionicPopup.show({
				template: '<textarea placeholder="'+$translate.instant('commentmodal_desc')+'" style="direction:rtl;" rows="4" cols="50" ng-model="data.text" ></textarea>',
				
				title: $translate.instant('commentmodal_desc'),
				//subTitle: 'Please use normal things',
				scope: $scope,
				buttons: [
				  { text: $translate.instant('canceltextctrl') },
				  {
					text: '<b>'+$translate.instant('sendtextctrl')+'</b>',
					type: 'button-positive',
					onTap: function(e) {
					  if (!$scope.data.text) 
					  {
						//e.preventDefault();
					  } else 
					  {
						  
						$ionicLoading.show({
						  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
						});							  

						$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

						send_params = 
						{
							"user" : $localStorage.userid,
							"supplier_id" : $scope.Sapak.index,
							"name": $localStorage.name,
							"text" : $scope.data.text,
							"send" : 1
						}

						$http.post($rootScope.Host+'/send_comment.php',send_params)
						.success(function(data, status, headers, config)
						{
							$ionicLoading.hide();
							
							$ionicPopup.alert({
								 title: $translate.instant('thankyoucommentctrl'),
								 template: '',
								okText: $translate.instant('oktextctrl'),	
							   });							
						})
						.error(function(data, status, headers, config)
						{

						});		

				
						
					  }
					}
				  }
				]
			  });			 
		 }


  
	/*
		$ionicModal.fromTemplateUrl('templates/comment_modal.html', {
		  scope: $scope,
		  animation: 'slide-in-up'
		}).then(function(ModalComment) {
		  $scope.ModalComment = ModalComment;
		  $scope.ModalComment.show();
		 });		
	*/
	}
	$scope.CloseCommentModal = function()
	{
		$scope.ModalComment.hide();
	}

	
	 $scope.starClick = function (nm1)
	 {
		 for(i=1;i<=nm1;i++)
		 {
			 //console.log("Full : " + i)
			 $scope.RateArray["rate"+i] = $scope.Full;
		 }
		 
		 for(i=5; i>nm1;i--)
		 {
			//console.log("Empty : " + i)
			$scope.RateArray["rate"+i] = $scope.Empty; 
		 }
		 
		 $scope.RateArray["val"] = nm1;
		 $scope.Rate = nm1;
		 
		 //console.log($scope.RateArray)
		// console.log($scope.RateArray["val"] + " : " + $scope.nm1)
	 }
	 
	 $scope.checkRate = function(type)
	 {
		if ($scope.checkLoggedIn(type))
		 {
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			
			send_params = 
			{
				"user" : $localStorage.userid,
				"supplier" : $scope.Sapak.index,
				"send" : 1
			}
						

			$http.post($rootScope.Host+'/check_ratings.php',send_params)
			.success(function(data, status, headers, config)
			{
				if (data[0].status == 0)
				{
					$scope.RateSupplier(type);
				}
				else
				{
					$ionicPopup.alert({
						 title: $translate.instant('cantratetwicectrl'),
						 template: '',
						 okText: $translate.instant('oktextctrl'),	
					   });					
				}
			})
			.error(function(data, status, headers, config)
			{

			});				 
		 }
			
	 }

	 
	$scope.RateSupplier = function(type)
	{
		$scope.BackModalType = type; 
		 if ($scope.checkLoggedIn(type))
		 {
			 // $scope.rate = {};

			  // An elaborate, custom popup
			   $ionicPopup.show({
				//template: '<input type="text" ng-model="rate.points" >',
				  templateUrl: 'templates/rate.html',
				title: $translate.instant('inputderugctrl'),
				//subTitle: 'Please use normal things',
				scope: $scope,
				buttons: [
				  { text: $translate.instant('canceltextctrl') },
				  {
					text: '<b>'+$translate.instant('oktextctrl')+'</b>',
					type: 'button-positive',
					onTap: function(e) {
						if ($scope.Rate > 0) {
							
						$ionicLoading.show({
						  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
						});	
						
						//e.preventDefault();
						$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

						
						send_params = 
						{
							"user" : $localStorage.userid,
							"rating" : $scope.Rate,
							"supplier" : $scope.Sapak.index,
							"send" : 1
						}
									

						$http.post($rootScope.Host+'/send_rating.php',send_params)
						.success(function(data, status, headers, config)
						{
							$ionicLoading.hide();
							$ionicPopup.alert({
								 title: $translate.instant('thankyouderugctrl'),
								 template: '',
								okText: $translate.instant('oktextctrl'),	
							   });	

						})
						.error(function(data, status, headers, config)
						{

						});						

					  } 
					  else 
					  {	
						$ionicPopup.alert({
							 title: $translate.instant('selectderugctrl'),
							 template: '',
							 okText: $translate.instant('oktextctrl'),	
						   });						    
					  }
					}
				  }
				]
			  });				 
		 }
	
	}


	$scope.dialPhone = function()
	{
		//alert ($scope.Sapak.phone)
		
		//window.location ="tel:"+$scope.Sapak.phone;
		if ($scope.Sapak.phone)
			window.location ="tel://"+$scope.Sapak.phone;
		
		//alert ($scope.Sapak.phone)
		
	}
	$scope.Waze = function()
	{
		

		if ($scope.Sapak.location_lat)
		{
			$scope.waze = $scope.Sapak.location_lat+","+$scope.Sapak.location_lng;
			window.location = "waze://?q="+$scope.waze+"&navigate=yes";			
		}

	

		//window.location = "http://waze.to/?ll="+$scope.Sapak.waze+"&navigate=yes";
	}
	$scope.Video = function()
	{
		if ($scope.Sapak.video)
		{
				var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]{11,11}).*/;
				var match = $scope.Sapak.video.match(regExp);
				if (match[2])
				{
					$scope.newVideoLink = "https://www.youtube.com/embed/"+match[2];
					
					$scope.VideoLink = $sce.trustAsResourceUrl($scope.newVideoLink);

					$ionicModal.fromTemplateUrl('video-modal.html', {
					  scope: $scope,
					  animation: 'slide-in-up'
					}).then(function(VideoModal) {
					  $scope.VideoModal = VideoModal;
					  $scope.VideoModal.show();
					 });					
				}


			/*	
			$ionicModal.fromTemplateUrl('templates/video_modal.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(VideoModal) {
			  $scope.VideoModal = VideoModal;
			  $scope.VideoModal.show();
			 });			
			*/
		}
		else
		{
			$ionicPopup.alert({
				 title: $translate.instant('novideofoundtrylaterctrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),	
			   });			
		}
		

		 
		//window.location  = ;
	}
	
	$scope.stopVideo = function()
	{
		var div = document.getElementById('videoWrapper');
		var player = document.getElementById('VideoPlayer');
		div.innerHTML = '';
		div.remove();
		player.src='';

	}

			
	$scope.closeVideoModal= function()
	{
		$scope.VideoModal.hide();
		$scope.stopVideo();
	}
	


	
	$rootScope.$watch('DefaultLanguage', function () 
	{   
		
		$scope.currentLanguage = $rootScope.DefaultLanguage;
		

		 $timeout(function() {
			 
		
			$scope.currentLanguage = $rootScope.DefaultLanguage;
			
			
			$scope.DaysWeek = new Array(
			  $translate.instant('daysoftheweek1ctrl'),
			  $translate.instant('daysoftheweek2ctrl'),
			  $translate.instant('daysoftheweek3ctrl'),
			  $translate.instant('daysoftheweek4ctrl'),
			  $translate.instant('daysoftheweek5ctrl'),
			  $translate.instant('daysoftheweek6ctrl'),
			  $translate.instant('daysoftheweek7ctrl')
			);


			
			$scope.reversedate =  $scope.Appoinement.start_date.split("-").reverse().join("/");;
			$scope.splitdate = $scope.reversedate.split("/");
			$scope.formatteddate = $scope.splitdate[2]+'-'+$scope.splitdate[1]+'-'+$scope.splitdate[0];
			$scope.NewDateFormat = new Date($scope.formatteddate);
			
			$scope.DayName = $scope.DaysWeek[$scope.NewDateFormat.getDay()];

		
		}, 300);



	});	


	
			  
	// },300);
	//})
})


.controller('searchAppoinmentCtrl', function($scope, $stateParams,loadMainJson,$rootScope,dateFilter,$ionicLoading,$state,$http,$localStorage,$ionicPopup,$timeout,$translate) 
{
	$scope.CatIndex = $stateParams.CatIndex;
	$scope.IndexArray = $stateParams.IndexArray;
	$scope.host = $rootScope.Host;
	$scope.selectedCategory = $rootScope.selectedCategory;
	$scope.isFutureMessage = 0;
	
	
	
	
	$scope.currentLanguage = $rootScope.DefaultLanguage;
	
	$rootScope.$watch('DefaultLanguage', function () 
	{   
		$scope.currentLanguage = $rootScope.DefaultLanguage;
		
		$timeout(function() {
			
			$scope.DaysWeek = new Array(
			  $translate.instant('daysoftheweek1ctrl'),
			  $translate.instant('daysoftheweek2ctrl'),
			  $translate.instant('daysoftheweek3ctrl'),
			  $translate.instant('daysoftheweek4ctrl'),
			  $translate.instant('daysoftheweek5ctrl'),
			  $translate.instant('daysoftheweek6ctrl'),
			  $translate.instant('daysoftheweek7ctrl')
			);
			
			$scope.reversedate =  $scope.appt.day.split("-").reverse().join("/");;
			$scope.splitdate = $scope.reversedate.split("/");
			$scope.formatteddate = $scope.splitdate[2]+'-'+$scope.splitdate[1]+'-'+$scope.splitdate[0];
			$scope.NewDateFormat = new Date($scope.formatteddate);
			
			$scope.DayName = $scope.DaysWeek[$scope.NewDateFormat.getDay()];

		}, 300);	


	});		
	

	
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
		dd='0'+dd
	} 

	if(mm<10) {
		mm='0'+mm
	} 

	$scope.today = dd+'/'+mm+'/'+yyyy;

	

	$scope.appt = 
	{
		"day" : $scope.today, //"28/01/2016",
		"time" : "" , // "12:00", // "בחר שעה",  //
		"subcat" : $scope.selectedCategory
	}

	
	$scope.pickTime = function()
	{
		var options = {
			date: new Date(),
			mode: 'time',
			androidTheme: 2,
			is24Hour : true,
			okText : $translate.instant('datepickoktext'),
			cancelText: $translate.instant('datepickcanceltext')			
		};
		/*
		function onSuccess(date) {

		}

		function onError(error) { // Android only
			//alert('Error: ' + error);
		}
		*/

		//datePicker.show(options, onSuccess, onError);		
		
		datePicker.show(options, function(date){
		  //alert("date result " + date);  
			$scope.newHour = date.toString().split(" ")[4];
			$scope.splitHour = $scope.newHour.split(":");
			$scope.newTime = $scope.splitHour[0]+":"+$scope.splitHour[1];
			$scope.appt.time = $scope.newTime;
			$scope.updateTime();
		
		});

		
	}

	$scope.updateTime = function()
	{
		 $timeout(function() {
			 
			$scope.appt.time = $scope.newTime;
		}, 100);
	}

	
	
/*
	for (var i=0; i<$rootScope.sapakSubcat.length; i++) {
		if ($rootScope.sapakSubcat[i].index == $rootScope.searchParams.catagory)
		{
			//$scope.appt.subcat = $rootScope.sapakSubcat[i].index;
		}
	}
*/

	//if ($rootScope.searchParams.catagory)
	//$scope.appt.subcat = $rootScope.searchParams.catagory;	


	if ($rootScope.NavigateMainSearch =="")
	{
		$scope.SapakSubcat = $rootScope.sapakSubcat;
	}
	else
	{
		$scope.SapakSubcat = $rootScope.MainSearchArray[$scope.IndexArray].subcat;
	}
	
	
	
	

	if ($rootScope.NavigateMainSearch == "")
	{
			for(i=0;i< $rootScope.SearchJson.length;i++)
			{
				//alert ($rootScope.SearchJson[i].index);
				if ($rootScope.SearchJson[i].index == $scope.IndexArray)
				{
					$scope.Sapak = $rootScope.SearchJson[i];
				}
			}
		
			
			
		$scope.SupplierName = $scope.Sapak.name;
		$scope.SupplierImage = $scope.Sapak.image;
		$scope.SupplierIndex = $scope.Sapak.index;	
		$scope.newSubCat = $scope.Sapak.newsubcat;
	}
	else
	{
		$scope.SupplierName = $rootScope.MainSearchArray[$scope.IndexArray].name;
		$scope.SupplierImage = $rootScope.MainSearchArray[$scope.IndexArray].image;
		$scope.SupplierIndex = $rootScope.MainSearchArray[$scope.IndexArray].index;		
		$scope.newSubCat = $rootScope.MainSearchArray[$scope.IndexArray].newsubcat;
		console.log("new sapak 333 : " , $scope.newSubCat)
	}


	console.log("Sapak: ", $scope.Sapak)
	

	//alert ($rootScope.searchParams.catagory);
	
	$scope.searchAppoint = function()
	{
		if ($scope.appt.time =="")
		{
			$ionicPopup.alert({
			 title: $translate.instant('selecttimectrl'),
			 template: '',
			 okText: $translate.instant('oktextctrl'),	
		   });			
		}
		else if ($scope.appt.subcat == 0)
		{
			$ionicPopup.alert({
			 title: $translate.instant('selectcategoryctrl'),
			 template: '',
			 okText: $translate.instant('oktextctrl'),	
			});		
		}		
		else
		{
			//$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
			$scope.newdate = dateFilter($scope.appt.day, 'dd/MM/yyyy');
			//alert ($scope.newdate)
			//console.log("Sub : " , $scope.appt.subcat)

			$scope.DaysWeek = new Array(
			  $translate.instant('daysoftheweek1ctrl'),
			  $translate.instant('daysoftheweek2ctrl'),
			  $translate.instant('daysoftheweek3ctrl'),
			  $translate.instant('daysoftheweek4ctrl'),
			  $translate.instant('daysoftheweek5ctrl'),
			  $translate.instant('daysoftheweek6ctrl'),
			  $translate.instant('daysoftheweek7ctrl')
			);

			//05/03/2017
			$scope.reversedate =  $scope.appt.day.split("-").reverse().join("/");;
			$scope.splitdate = $scope.reversedate.split("/");
			$scope.formatteddate = $scope.splitdate[2]+'-'+$scope.splitdate[1]+'-'+$scope.splitdate[0];
			$scope.NewDateFormat = new Date($scope.formatteddate);
			$scope.DayName = $scope.DaysWeek[$scope.NewDateFormat.getDay()];
			

		console.log("Appt : " ,$scope.appt )

				$ionicLoading.show({
				  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
				});		
		
				search_params = 
				{
					"supplier" : $scope.SupplierIndex,
					"subcat" : $scope.appt.subcat,
					"day" : $scope.newdate,
					"time" : $scope.appt.time,
					"type" : 0,
				}
				
				console.log('params:', search_params);
				//alert ($scope.appt.subcat)
			//	console.log("Sapp : " , search_params)
			
				//2
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
				$http.post('http://tapper.co.il/mami/laravel/public/SearchAppointmentByUser',search_params)
				.success(function(data, status, headers, config)
				{
					console.log('results:', data);
					$ionicLoading.hide();
					
					if (data == "0")
					{
							$ionicPopup.alert({
							 title: $translate.instant('nomatchestrydifftimehourctrl'),
							 template: '',
							 okText: $translate.instant('oktextctrl'),	
						   });
						   
						$rootScope.SearchAppointment = "";
						$scope.Appoinments = "";
						$scope.AppointPrice = "";						   
					}
					else if (data == "-1")
					{
						$ionicPopup.alert({
						 title: $translate.instant('nofuturetortext'),
						 template: '',
				         okText: $translate.instant('oktextctrl'),	
					   });	

						$rootScope.SearchAppointment = "";
						$scope.Appoinments = "";
						$scope.AppointPrice = "";

					
					}
					else
					{
						$rootScope.SearchAppointment = data;
						$scope.Appoinments = data;
						console.log('results:', data);
						if ($scope.Appoinments)
						$scope.AppointPrice = $scope.Appoinments[0].subcategories[0].price;
					
					
						for(i=0;i<$scope.Appoinments.length;i++)
						{
							if($scope.Appoinments[i].isFuture == 1)
							$scope.isFutureMessage = 1;
							else
							$scope.isFutureMessage = 0;
						}
						
						//alert (data);
						//$state.go('app.list',{ItemId:$scope.ItemId});						
					}
					
					


				})
				.error(function(data, status, headers, config)
				{
					console.log(data ,status)
					$ionicLoading.hide();
				});				
		}
	
		

	}
	
	$scope.saveAppoint = function(item)
	{
		id = item.index
		supplier = item.supplier_index
		
		
	   var confirmPopup = $ionicPopup.confirm({
		 title: $translate.instant('confirmtorctrl'),
		 template: '',
		 cancelText: $translate.instant('canceltextctrl'), 
		 okText: $translate.instant('oktextctrl')	 
	   });
   
	   $scope.SubCut = item.subcategories;
	   console.log("SB : " , item.subcategories)
	   for(i=0;i<$scope.SubCut.length;i++)
	   {
			if($scope.appt.subcat == $scope.SubCut[i].subcat_index)
			{
				item.len = $scope.SubCut[i].time;
				console.log("Appointment : " , item)
			}
	   }

	   confirmPopup.then(function(res) {
		 if(res) 
		 {
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
			
				send_params = 
				{
					"userId" : $localStorage.userid,
					"appointmentId" : id,
					"supplier_id" : supplier,
					"start":item.start_time,
					"len":item.len,
					"subcat":$scope.appt.subcat,
					"pushid" : $rootScope.pushId
				}
				
				console.log("Spp",send_params)
				$http.post('http://tapper.co.il/mami/laravel/public/saveAppointment',send_params)
				.success(function(data, status, headers, config)
				{
					console.log(data)
					$ionicPopup.alert({
						 title: $translate.instant('thankyoutorctrl'),
						 template: '',
						 okText: $translate.instant('oktextctrl'),	
					   });
   
					$scope.Appoinments = {};
				})
				.error(function(data, status, headers, config)
				{

				});	
			 } 
		 else 
		 {
		 }
	   });

	
	}
})

.controller('AppointmentsCtrl', function($scope, $stateParams,loadMainJson,$rootScope,dateFilter,$ionicLoading,$state,$http,$localStorage,$ionicPopup,$ionicModal,$timeout,$translate) 
{
//$scope.$on('$ionicView.enter', function(e) {
  
	$scope.TypeSelected = 1;
	$scope.host = $rootScope.Host;
	$scope.ActiveTab = 0;
	
	$scope.SearchSupplier = '';
	$scope.SearchIndex = '';
	$scope.SearchId = '';
	$scope.AppId = '';
	$scope.SupplierImage = '';
	$scope.SupplierName = '';
	$scope.AppoinmentCount = 0;
	

	
	$scope.currentLanguage = $rootScope.DefaultLanguage;
	
	$rootScope.$watch('DefaultLanguage', function () 
	{   
		$scope.currentLanguage = $rootScope.DefaultLanguage;

	});		
	
	$scope.search = 
	{
		"start" : "",
		"end" : ""
	}		
	
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; 

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 
 
	$scope.today = new Date(mm+'/'+dd+'/'+(yyyy)) ;
	$scope.todaynew = dd+'/'+mm+'/'+yyyy;
	$scope.todaynewdate = yyyy+'-'+mm+'-'+dd;
	
	//alert ($scope.today)

	$scope.appt = 
	{
		"day" : $scope.todaynew,
		"time" : "15:00",
		"subcat" : "212"
	}

	$scope.pickTime = function()
	{
		var options = {
			date: new Date(),
			mode: 'time',
			androidTheme: 2,
			is24Hour : true,
			okText : $translate.instant('datepickoktext'),
			cancelText: $translate.instant('datepickcanceltext')			
		};
		/*
		function onSuccess(date) {

		}

		function onError(error) { // Android only
			//alert('Error: ' + error);
		}
		*/

		//datePicker.show(options, onSuccess, onError);		
		
		datePicker.show(options, function(date){
		  //alert("date result " + date);  
			$scope.newHour = date.toString().split(" ")[4];
			$scope.splitHour = $scope.newHour.split(":");
			$scope.newTime = $scope.splitHour[0]+":"+$scope.splitHour[1];
			$scope.appt.time = $scope.newTime;
			$scope.updateTime();
		
		});
	}


		$scope.updateTime = function()
		{
		 $timeout(function() {
			$scope.appt.time = $scope.newTime;
		}, 100);
	}
	
	
	
	$scope.changeType = function(Type)
	{
		$scope.TypeSelected = Type;
	}

	$scope.setActiveTab = function(tab)
	{
		$scope.ActiveTab = tab;
			

			if (tab == 0)
			{

				$scope.getUserAppoinmenets(tab);
			}
			if (tab == 1)
			{

				$scope.getUserAppoinmenets(tab);
			}
			if (tab == 2)
				$scope.Appointments = [];
				
				console.log($scope.search.start + " : "  + $scope.search.end)
			
	}

	
	
		
	
	$scope.getUserAppoinmenets = function(type)
	{
		
		
		$ionicLoading.show({
		  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
		});			
		
		
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
			
			
			
			send_params = 
			{
				"user" : $localStorage.userid,
				"start" : $scope.search.start ,
				"end" : $scope.search.end,
				"type" : type//0				
			}
			
			
			console.log("Data001 : " , send_params)
			$scope.Url = 'http://tapper.co.il/mami/laravel/public/getHistory';		

			//$http.post($rootScope.Host+'/get_user_appointments.php',send_params)
			$http.post($scope.Url,send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				console.log("DT : " , data)
				$scope.newAppoinment = 0;
				$scope.oldAppoinment = 0;
				$scope.Appointments = data;
				$scope.AppoinmentCount = data.length;
				console.log("user appoinments: ", $scope.Appointments);
				//alert (data.length);
				
			})
			.error(function(data, status, headers, config)
			{

			});			
	}
	
	$scope.setActiveTab(0);

	$scope.searchDate= function()
	{
		if ($scope.search.start =="")
		{
			$ionicPopup.alert({
			 title: $translate.instant('inputstartdatectrl'),
			 template: '',
		     okText: $translate.instant('oktextctrl'),	
			});
		}
		else if ($scope.search.end =="" )
		{
			$ionicPopup.alert({
			 title: $translate.instant('inputenddatectrl'),
			 template: '',
		     okText: $translate.instant('oktextctrl'),	
			});
		}
		else
		{
			$scope.getUserAppoinmenets(2);
		}
	}
	

	

	
	$scope.deleteAppoint = function(appid)
	{
	   var confirmPopup = $ionicPopup.confirm({
		 title: '<p dir="'+$rootScope.lanDirction+'">'+$translate.instant('deletetorconfirmctrl')+'</p>',		   
		 template: '',
		 cancelText: $translate.instant('canceltextctrl'), 
		 okText: $translate.instant('oktextctrl') 
	   });

	   confirmPopup.then(function(res) {
		 if(res) 
		 {

	 
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
			
			send_params = 
			{
				"appId" : appid,
			}
			console.log("DelAppointment : " , appid)
			$http.post($rootScope.laravelHost+'DeleteAppointment',send_params)
			
			.success(function(data, status, headers, config)
			{
				console.log("Delete : " , data , " : " ,$scope.itemId)
				$scope.isBookedAppointment = 0;
				$scope.getUserAppoinmenets(0);
			})
			.error(function(data, status, headers, config)
			{
			});		

		
		  } 

	   });
	   
	   
	
	}
	
	$scope.searchByDate= function(value)
	{
		if (value)
		{
			if (value.appoinement[0].start_date > $scope.today)
			{
				return 0;
			}
			else
			{
				return 1;
			}			
		}

	}


	
	$scope.changeDate = function(index,id,supplier,suppliername,image,appid,subcat)
	{
		$scope.getSubCategoryById(supplier)
		$scope.appt.subcat = subcat;
		$scope.SearchSupplier = supplier;
		$scope.SearchIndex = index;
		$scope.SearchId = id;
		$scope.AppId = appid;
		$scope.SupplierImage = image;
		$scope.SupplierName = suppliername;
		$scope.Appoinments = [];
		
		console.log(index + " : " + id + " : " + supplier + " : " + suppliername + " : " + image + " : " + appid)
		  $ionicModal.fromTemplateUrl('templates/search_modal.html', {
			scope: $scope
		  }).then(function(searchAppoinment) {
			$scope.searchAppoinment = searchAppoinment;
			$scope.searchAppoinment.show();
		  });	
	}
	
	$scope.getSubCategoryById = function(supplierId)
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		
		send_params = 
		{
			"supplierId" : supplierId,
		}
			
			
	    $http.post('http://tapper.co.il/mami/laravel/public/getSubCategory',send_params)
		.success(function(data, status, headers, config)
		{
			$scope.newSubCat = data;
			console.log("Sub " , data)
		})
		.error(function(data, status, headers, config)
		{
				
		});			
	}
		
		
	$scope.closeSearchModal = function()
	{
		$scope.searchAppoinment.hide();
		$scope.Appoinments = [];
	}

	$scope.searchAppoint = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		$scope.newdate = dateFilter($scope.appt.day, 'dd/MM/yyyy');
		//alert ($scope.newdate)
		

		$scope.DaysWeek = new Array(
		  $translate.instant('daysoftheweek1ctrl'),
		  $translate.instant('daysoftheweek2ctrl'),
		  $translate.instant('daysoftheweek3ctrl'),
		  $translate.instant('daysoftheweek4ctrl'),
		  $translate.instant('daysoftheweek5ctrl'),
		  $translate.instant('daysoftheweek6ctrl'),
		  $translate.instant('daysoftheweek7ctrl')
		);

		$scope.NewDate = new Date($scope.appt.day);
		$scope.DayName = $scope.DaysWeek[$scope.NewDate.getDay()];

		
			$ionicLoading.show({
			  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
			});			
	

			search_params = 
			{
				"supplier" : $scope.SearchSupplier,
				"subcat" : $scope.appt.subcat,
				"day" : $scope.newdate,
				"time" : $scope.appt.time,
				"type" : 0,
			}
			
			console.log("s : " , search_params , $scope.appt)
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';

			//$http.post($rootScope.Host+'/search_Appointment.php',search_params)
			
			
		
			//1
			$http.post('http://tapper.co.il/mami/laravel/public/SearchAppointmentByUser',search_params)
			
			
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				//alert(JSON.stringify(data));
				//alert (data);

				if (data == 0)
				{
					$ionicPopup.alert({
						 title: $translate.instant('nomatchestrydifftimehourctrl'),
						 template: '',
						 okText: $translate.instant('oktextctrl'),	
					   });
					   
					$rootScope.SearchAppointment = [];
					$scope.Appoinments = [];
					$scope.AppointPrice = "";

				}
				else if (data == -1)
				{
					$ionicPopup.alert({
						 title: $translate.instant('nofuturetortext'),
						 template: '',
						 okText: $translate.instant('oktextctrl'),	
					   });
					   
					$rootScope.SearchAppointment = [];
					$scope.Appoinments = [];
					$scope.AppointPrice = "";					
				}
				
				else
				{
					$rootScope.SearchAppointment = data;
					$scope.Appoinments = data;
					console.log('results:', data);
					if ($scope.Appoinments)
					$scope.AppointPrice = $scope.Appoinments[0].subcategories[0].price;					
				}
				

				//alert (data);
				//$state.go('app.list',{ItemId:$scope.ItemId});

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});			
	}

	$scope.saveAppoint = function(id,supplier,index)
	{
		
	   var confirmPopup = $ionicPopup.confirm({
		 title: '<p dir="'+$rootScope.lanDirction+'">'+$translate.instant('confirmchangetorctrl')+'</p>',		   
		 template: '',
		 cancelText: $translate.instant('canceltextctrl'), 
		 okText: $translate.instant('oktextctrl') 
	   });

	   confirmPopup.then(function(res) {
		 if(res) 
		 {
	
			 $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
			
				/*send_params = 
				{
					"user" : $localStorage.userid,
					"app_id" : id,
					"supplier_id" : supplier,
					"old_id" : $scope.SearchId,
					"old_apt" : $scope.AppId
				}*/
				console.log("Details : " , id,":",supplier,":",index)
				  $scope.SubCut = $scope.newSubCat;
				
				   for(i=0;i<$scope.SubCut.length;i++)
				   {
						if($scope.appt.subcat == $scope.SubCut[i].subcat_index)
						{
							$scope.SubLen = $scope.SubCut[i].time;
							console.log("Appointment : " , $scope.SubLen ,index)
						}
				   }
	   
					send_params = 
					{
						"userId" : $localStorage.userid,
						"appointmentId" : $scope.Appoinments[index].index,
						"supplier_id" : $scope.Appoinments[index].supplier_index,
						"start":$scope.Appoinments[index].start_time,
						"len":$scope.SubLen,
						"subcat":$scope.appt.subcat,
						"pushid" : $rootScope.pushId

					}
				
				
				//alert (444);
				//return;
				
				console.log("sAppoint = " , $scope.SearchId)
				//$http.post($rootScope.Host+'/update_appoinment.php',send_params)
				$http.post('http://tapper.co.il/mami/laravel/public/saveAppointment',send_params)
				.success(function(data, status, headers, config)
				{
					$ionicPopup.alert({
						 title: $translate.instant('thankyoutorctrl'),
						 template: '',
						 okText: $translate.instant('oktextctrl'),	
					   });
   					$scope.delAppoint($scope.SearchId)
					$scope.closeSearchModal();
					$scope.getUserAppoinmenets();
				})
				.error(function(data, status, headers, config)
				{

				});	
			 } 
		 else 
		 {
		 }
	   });

	
	}	
	
	
	$scope.delAppoint = function(appid)
	{
	  
	  		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
			
			send_params = 
			{
				"appId" : appid,
			}
			console.log("DelAppointment12 : " , appid)
			$http.post($rootScope.laravelHost+'DeleteAppointment',send_params)
			
			.success(function(data, status, headers, config)
			{
				console.log("Delete : " , data , " : " ,$scope.itemId)
				$scope.isBookedAppointment = 0;
				$scope.getUserAppoinmenets(0);
			})
			.error(function(data, status, headers, config)
			{
			});		

		
		 
	   
	
	}
	
	/*$scope.myFilter = function (item) 
	{ 
		$scope.DaysArray = $scope.CurrentDay.split("-");
		$scope.CurrentDay1 = $scope.DaysArray[0] + "/"+ $scope.DaysArray[1] +"/"+ $scope.DaysArray[2];
		//console.log("Filter : " + item.start_date + " : "  + $scope.CurrentDay1 )
		if(String(item.start_date) == String($scope.CurrentDay1))
		{
    		return item;
		}
	};*/
	
	$scope.CheckTime = function(item)
	{
		var DaysArray = item.start_date.split("/");
		var CurrentDay = DaysArray[1] + "/"+ DaysArray[0] +"/"+ DaysArray[2] +"   "+ item.start_time ;
		//var strDate = '01/28/2017 20:00'
		var datum = new Date(CurrentDay).getTime()
		
		if(Math.floor(Date.now() / 1000) <= datum )
		item.ishistory = 1; 
		else
		item.ishistory = 0
		
		console.log(item.ishistory)
  		//console.log(CurrentDay + " : "  + item.start_date + " : " + datum/1000 + " : "  + Math.floor(Date.now() / 1000));
		return item;
	}
//};
})



.controller('SupplierCtrl', function($scope, $stateParams,loadMainJson,$rootScope,$ionicLoading,$state,$http,$localStorage,$ionicPopup,$sce,$translate) 
{
	
	$scope.supplier = 
	{
		"name" : "",
		"phone" : "",
		"mail" : "",
		"desc" : "",
		"username" : "",
		"password" : ""
	}
	

	
	
	$http.get($rootScope.Host+'/supplierMain.php')
	.success(function(data, status, headers, config)
	{
		var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]{11,11}).*/;
		var match = data['video'][0].value.match(regExp);

		$scope.newsupplierVideo = "https://www.youtube.com/embed/"+match[2]+'?showinfo=0&autohide=2&modestbranding=1&rel=0';
		
		$scope.MainVideo = $sce.trustAsResourceUrl($scope.newsupplierVideo);
		$scope.Questions = data.questions;
	})
	.error(function(data, status, headers, config)
	{

	});

	
	$scope.sendLead = function()
	{
		if ($scope.supplier.name =="")
		{
			$ionicPopup.alert({
				 title: $translate.instant('inputfullnamectrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),	
			   });			
		}
		else if ($scope.supplier.phone =="")
		{
			$ionicPopup.alert({
				 title: $translate.instant('inputphonectrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),	
			   });			
		}
		else if ($scope.supplier.mail =="")
		{
			$ionicPopup.alert({
				 title: $translate.instant('inputemailctrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),	
			   });			
		}		
		else
		{

			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
			send_params = 
			{
				"name" : $scope.supplier.name,
				"phone" : $scope.supplier.phone,
				"mail" : $scope.supplier.mail,
				"desc" : $scope.supplier.desc,
				"send" : 1
			}			
			$http.post($rootScope.Host+'/send_lead.php',send_params)
			.success(function(data, status, headers, config)
			{

			})
			.error(function(data, status, headers, config)
			{

			});	
			
			$ionicPopup.alert({
				 title: $translate.instant('formsentctrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),	
			   });
					   
			$scope.supplier.name = '';
			$scope.supplier.phone = '';
			$scope.supplier.mail = '';
			$scope.supplier.desc = '';
	
		}
	
	}

	$scope.stopVideo = function()
	{
		var div = document.getElementById('videoWrapper');
		var player = document.getElementById('VideoPlayer');
		div.innerHTML = '';
		div.remove();
		player.src='';
		//player.pause(); 

	}

	
	
	$scope.$on("$ionicView.afterLeave", function(event, data){
	   
		//$scope.stopVideo();
	});

	

})

.controller('SupplierLoginCtrl', function($scope, $stateParams,loadMainJson,$rootScope,$ionicLoading,$state,$http,$localStorage,$ionicPopup,$sce,$translate,$ionicHistory) 
{


    if ($localStorage.supplierid)
    {
        $ionicHistory.nextViewOptions({
                disableAnimate: true,
                expire: 300,
				disableBack: true
            });
    
        $state.go('app.suppliermain');
    }
	
	
	$scope.supplier = 
	{
		"name" : "",
		"phone" : "",
		"mail" : "",
		"desc" : "",
		"username" : "",
		"password" : ""
	}
	
	$scope.supplierConnect = function()
	{
		if ($scope.supplier.username =="")
		{
			$ionicPopup.alert({
				 title: $translate.instant('inputusernamectrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),	
			   });			
		}
		else if ($scope.supplier.password =="")
		{
			$ionicPopup.alert({
				 title: $translate.instant('inputpasswordctrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),	
			   });			
		}
		else
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			send_params = 
			{
				"username" : $scope.supplier.username,
				"password" : $scope.supplier.password,
				"pushid" : $rootScope.pushId,
				"send" : 1
			}			
			$http.post($rootScope.Host+'/supplier_login.php',send_params)
			.success(function(data, status, headers, config)
			{
				if (data.response.status == 1)
				{
					$localStorage.supplierid = data.response.id;
					//$scope.getSupplierDetails()
					window.location = "#/app/suppliermain";
				}
				else
				{
					$ionicPopup.alert({
					 title: $translate.instant('wrongusernamectrl'),
					 template: '',
					okText: $translate.instant('oktextctrl'),	
					});			
							
				}
				
			})
			.error(function(data, status, headers, config)
			{

			});	
					   
		}
	}
	
	
	$scope.getSupplierDetails = function()
	{
		$scope.newdate = dateFilter($scope.search.day, 'dd/MM/yyyy');
		
		send_params = 
		{
			"catagory" : $scope.CatagoryIndex,
			"area" : $scope.search.area,
			"subcat" : $scope.search.subcat,
			"day" : $scope.newdate,
			"time" : $scope.search.time,
			"type" : $scope.show,
			"queu_len" : 22
		}
				
				//console.log("Today1 : " ,$scope.today + " : " + $scope.newdate)
				console.log("PPP : " , send_params)
				
				if ($scope.show == 1)
				{
					$scope.Url = $rootScope.Host+'/general_search.php';
				}
				else
				{
					$scope.Url = 'http://tapper.co.il/mami/laravel/public/searchAppointment';
				}
				
				$http.post($scope.Url,send_params)
				//$http.post($rootScope.Host+'/'+$scope.Url,send_params)
				.success(function(data, status, headers, config)
				{
					$rootScope.SelectedSubCategory = $scope.search.subcat;
					$ionicLoading.hide();
					$rootScope.SearchJson = data;
					console.log('search results: ' , data)
					
					if ($rootScope.SearchJson != 0)
					window.location = "#/app/list/"+$scope.ItemId;
					else 
					{
						$ionicPopup.alert({
							 title: $translate.instant('nomatchestryagainctrl'),
							 template: '',
							okText: $translate.instant('oktextctrl'),	
						   });	
					}
					
				})
				.error(function(data, status, headers, config)
				{
					$ionicLoading.hide();
				});			
				
	}
})

.controller('SupplierMain', function($scope, $stateParams,loadMainJson,$rootScope,$ionicLoading,$state,$http,$localStorage,$ionicPopup,$sce) 
{
	$scope.host = $rootScope.Host;
	
	
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
			send_params = 
			{
				"supplier_id" : $localStorage.supplierid,
				"send" : 1
			}			
			$http.post($rootScope.Host+'/supplier_data.php',send_params)
			.success(function(data, status, headers, config)
			{
			   $scope.SupplierDetails = data.response;
			})
			.error(function(data, status, headers, config)
			{
			});	

	$scope.SupplierLogout = function()
	{
		$localStorage.supplierid = '';
		window.location = "#/app/main";
	}
			
})
.controller('SupplierPersonal', function($scope, $stateParams,loadMainJson,$rootScope,$ionicLoading,$state,$http,$localStorage,$ionicPopup,$sce,$cordovaCamera,$timeout,$translate) 
{
	$scope.personal = 
	{
	  "address" : "",
	  "area" : "",
	  "email" : "",
	  "phone" : "",
	  "video" : "",
	  "facebook" : "",
	  "waze" : ""
	}
	
	$scope.host = $rootScope.Host;
	
	$scope.autocompleteOptions = {
	  componentRestrictions: { country: 'IL' }
	}
	
	
	$scope.CameraOptions = function()
	{
		var myPopup = $ionicPopup.show({
		//template: '<input type="text" ng-model="data.myData">',
		//template: '<style>.popup { width:500px; }</style>',
		title: $translate.instant('selectoptionctrl'),
		scope: $scope,
		cssClass: 'custom-popup',
		buttons: [


	   {
		text: $translate.instant('cameractrl'),
		type: 'button-positive',
		onTap: function(e) { 
		  $scope.takePicture(1);
		}
	   },
	   {
		text: $translate.instant('galleryctrl'),
		type: 'button-calm',
		onTap: function(e) { 
		 $scope.takePicture(0);
		}
	   },
	   	   {
		text: $translate.instant('canceltextctrl'),
		type: 'button-assertive',
		onTap: function(e) {  
		  //alert (1)
		}
	   },
	   ]
	  });		
	}

	$scope.takePicture = function(index,imagenumber) 
	{
		 var options ;
		 
		if(index == 1 )
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL, 
				sourceType : Camera.PictureSourceType.CAMERA, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
		else
		{
			options = { 
				quality : 75, 
				destinationType : Camera.DestinationType.FILE_URI, 
				sourceType : Camera.PictureSourceType.PHOTOLIBRARY, 
				allowEdit : false,
				encodingType: Camera.EncodingType.JPEG,
				targetWidth: 600,
				targetHeight: 600,
				popoverOptions: CameraPopoverOptions,
				saveToPhotoAlbum: false,
				correctOrientation: true
			};
		}
	 
        $cordovaCamera.getPicture(options).then(function(imageData) 
		{
			/*
			if(index == 1 )
			$scope.imgURI = "data:image/jpeg;base64," + imageData;
			else
			*/
			$scope.imgURI = imageData
			var myImg = $scope.imgURI;
			var options = new FileUploadOptions();
			options.mimeType = 'jpeg';
			options.fileKey = "file";
			options.fileName = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1);
			//alert(options.fileName)
			var params = {};
			//params.user_token = localStorage.getItem('auth_token');
			//params.user_email = localStorage.getItem('email');
			options.params = params;
			var ft = new FileTransfer();
			ft.upload($scope.imgURI, encodeURI($rootScope.Host+'/UploadImg1.php'), $scope.onUploadSuccess, $scope.onUploadFail, options);
    	});
		
		$scope.onUploadSuccess = function(data)
		{
			 $timeout(function() {
				if (data.response) {
				
					$scope.image = data.response;
				    $scope.personal.image = $scope.image;
					$scope.uploadedimage = $rootScope.Host+data.response;						
					//$scope.showimage = true;
				}
			}, 300);
			

		}
		
		$scope.onUploadFail = function(data)
		{
			alert("onUploadFail : " + data);
		}
    }

	
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
			send_params = 
			{
				"supplier_id" : $localStorage.supplierid,
				"send" : 1
			}			
			$http.post($rootScope.Host+'/supplier_personal.php',send_params)
			.success(function(data, status, headers, config)
			{
			   $scope.personal = data;
			   console.log($scope.personal)
			})
			.error(function(data, status, headers, config)
			{
			});	
			
			
			

	$scope.savePersonal = function()
	{
			if ($scope.personal.address.formatted_address)
			{
				$scope.formattedaddress = $scope.personal.address.formatted_address;
				$scope.newaddress = String($scope.personal.address.geometry.location);
				$scope.split = $scope.newaddress.split(",");
				$scope.locationx = $scope.split[0].replace("(", "");
				$scope.locationy = $scope.split[1].replace(")", "");
			
			}
			else
			{
				$scope.formattedaddress = $scope.personal.address;
				$scope.locationx = $scope.personal.location_lat;
				$scope.locationy = $scope.personal.location_lng;			
			}

		
		console.log("translate these:");
		
		if ($scope.formattedaddress =="" )
		{
			$ionicPopup.alert({
				 title: $translate.instant('fillinaddressctrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),	
			   });				
		}
		else if ($scope.personal.email =="")
		{
			$ionicPopup.alert({
				 title: $translate.instant('fillinemailctrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),	
			   });				
		}
		else if ($scope.personal.phone =="")
		{
			$ionicPopup.alert({
				 title: $translate.instant('fillinphonectrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),	
			   });				
		}
		else
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			
				send_params = 
				{
					"supplier_id" : $localStorage.supplierid,
					"address" : $scope.formattedaddress,
					"location_lat" : $scope.locationx,
					"location_lng" : $scope.locationy,
					"area" : $scope.personal.area,
					"email" : $scope.personal.email,
					"phone" : $scope.personal.phone,
					"video" : $scope.personal.video,
					"facebook" : $scope.personal.facebook,
					//"waze" : $scope.personal.waze,
					"send" : 1
				}			
				$http.post($rootScope.Host+'/save_personal.php',send_params)
				.success(function(data, status, headers, config)
				{
					$ionicPopup.alert({
					 title: $translate.instant('detailssavedctrl'),
					 template: '',
					okText: $translate.instant('oktextctrl'),	
					});	
					
					window.location = "#/app/suppliermain";
				})
				.error(function(data, status, headers, config)
				{
				});				
		}

		
		
			
	}
			
})

.controller('SupplierForgotCtrl', function($scope, $stateParams,loadMainJson,$rootScope,$ionicLoading,$state,$http,$localStorage,$ionicPopup,$sce, $ionicScrollDelegate,$translate) 
{
	$scope.supplier = 
	{
		"username" : "",
	}
	
	$scope.sendSupplierPass = function()
	{
		if ($scope.supplier.username =="")
		{
			$ionicPopup.alert({
			 title: $translate.instant('inputusernamectrl'),
			 template: '',
		     okText: $translate.instant('oktextctrl'),	
			});				
		}
		else
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			$http.post($rootScope.Host+'/forgot_supplier.php',$scope.supplier)
			.success(function(data, status, headers, config)
			{
				
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					 title: $translate.instant('usernamenotfoundtryagainctrl'),
					 template: '',
					 okText: $translate.instant('oktextctrl'),	
					});		

					$scope.supplier.username = '';	
				}
				else
				{
					$ionicPopup.alert({
					 title: $translate.instant('formsentctrl'),
					 template: '',
					 okText: $translate.instant('oktextctrl'),	
					});		

					$scope.supplier.username = '';
					window.location = "#/app/supplierLogin";					
				}
				
				
				
	

			})
			.error(function(data, status, headers, config)
			{
			});				
		}
	}
	
})	
	

.controller('SupplierAppointments', function($scope, $stateParams,loadMainJson,$rootScope,$ionicLoading,$state,$http,$localStorage,$ionicPopup,$sce, $ionicScrollDelegate,$translate) 
{

		$scope.ActiveTab = 0;
		$scope.newAppoinment = 0;
		$scope.oldAppoinment = 0;		
		$scope.AppoinmentCount = 0;
		$scope.currentType = 0;
		$scope.daysSearch = true;
		$scope.showSearch = false;
		$scope.appoinements = [];
		
		$scope.search = 
		{
			"start" : "",
			"end" : ""
		}



		
		
	
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; 
		var yyyy = today.getFullYear();

		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		} 

		$scope.today = dd+'/'+mm+'/'+yyyy;
		$scope.todaynewdate = yyyy+'-'+mm+'-'+dd;
		$scope.cDay = $scope.today;
	

		$scope.DaysWeek = new Array(
		  $translate.instant('daysoftheweek1ctrl'),
		  $translate.instant('daysoftheweek2ctrl'),
		  $translate.instant('daysoftheweek3ctrl'),
		  $translate.instant('daysoftheweek4ctrl'),
		  $translate.instant('daysoftheweek5ctrl'),
		  $translate.instant('daysoftheweek6ctrl'),
		  $translate.instant('daysoftheweek7ctrl')
		);		
		
		
		$scope.setActiveTab = function(tab)
		{
			$scope.ActiveTab = tab;
			
			$scope.ActiveTab = tab;
			if (tab == 0 || tab == 1)
			$scope.getDataFun(tab);
			if (tab == 2)
				$scope.appoinements = [];
			
		}

		$scope.changeType = function(type)
		{
			$scope.currentType = type;
			$scope.getDataFun($scope.ActiveTab);
		}

		
		$scope.searchByDate= function(value)
		{
			if (value)
			{
				if (value.start_date > $scope.today)
				{
					return 0;
				}
				else
				{
					return 1;
				}			
			}

		}
		
		$scope.pickDate = function()
		{
			var options = {
				date: new Date(),
				mode: 'date',
				androidTheme: 2,
				//minuteInterval: 10,
				titleText  : $translate.instant('search_date'),
				okText : $translate.instant('datepickoktext'),
				cancelText: $translate.instant('datepickcanceltext')
			};
			

			
			datePicker.show(options, function(date){
				//alert (date);
				
					var a = date;
					var b = a.getFullYear();
					var c = a.getMonth();
					(++c < 10)? c = "0" + c : c;
					var d = a.getDate();
					(d < 10)? d = "0" + d : d;
					var fulldate = b + "-" + c + "-" + d; 
					var finaldate = d + "/" + c + "/" + b; 
					if (finaldate)
					{
				
						//var finaldate = "2016-08-03";
						
						$scope.NewDate = new Date(fulldate);
						$scope.DayName = $scope.DaysWeek[$scope.NewDate.getDay()];
						//alert ($scope.DayName)
						$scope.CurrentDayText = $scope.DayName+' '+fulldate;
			
						$scope.daysSearch = false;
						$scope.showSearch = true;
						
						$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';


						
						send_params = 
							{
								"supplier_id" : $localStorage.supplierid,
								"start" : finaldate,
								//"end" : $scope.search.end,
								//"type" : type,				
								"send" : 1
							}			
							

							
							//alert ($scope.Url)
							$http.post($rootScope.Host+'/search_supplier_appoinments.php',send_params)
							.success(function(data, status, headers, config)
							{
								$scope.searchArray = data.response;
								$rootScope.ClientAppointments = $scope.searchArray;
								
								
								console.log("search:", $scope.searchArray)
								if (data.length == 0)
								{
									$ionicPopup.alert({
									 title: $translate.instant('notordatefoundctrl'),
									 template: '',
									okText: $translate.instant('oktextctrl'),	
									});									
								}

								
							})
							.error(function(data, status, headers, config)
							{
							})							
					}
					//02/08/2016
				
			});
		}

		$scope.getDataFun = function(type)
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		

			
			send_params = 
			{
				"supplier_id" : $localStorage.supplierid,
				"day":$scope.cDay
			}			

				$scope.Url = "GetAppointmentsByDay";

			
			//alert ($scope.Url)
			 console.log("URL : " , type , " : ",send_params);
			//$http.post($rootScope.Host+'/'+$scope.Url,send_params)
			
			$http.post($rootScope.laravelHost+'GetAppointmentsByDay',send_params)
		
			
			.success(function(data, status, headers, config)
			{
			   console.log("appoinements : " , data);
			  //alert (data.response);
			   $scope.appoinements = data;
			   $scope.AppoinmentCount = data.length;
			   $rootScope.ClientAppointments = data;
				
			})
			.error(function(data, status, headers, config)
			{
			})			
		}
		
		$scope.getDataFun(0);


		
	$scope.searchDate= function()
	{
		if ($scope.search.start =="")
		{
			$ionicPopup.alert({
			 title: $translate.instant('inputstartdatectrl'),
			 template: '',
			 okText: $translate.instant('oktextctrl'),	
			});
		}
		else if ($scope.search.end =="" )
		{
			$ionicPopup.alert({
			 title: $translate.instant('inputenddatectrl'),
			 template: '',
			 okText: $translate.instant('oktextctrl'),	
			});
		}
		else
		{
			$scope.getDataFun(2);
		}
	}

	
	$scope.deleteAppiont = function(index,id,booked)
	{
		
		if (booked == 0)
		  $scope.alertDesc = $translate.instant('activitytimectrl');
		else
			$scope.alertDesc = $translate.instant('tortextctrl');

		
		//alert (id);
	   var confirmPopup = $ionicPopup.confirm({
		 title: $translate.instant('confirmdeletectrl')+' '+$scope.alertDesc+'?',
		 template: '',
		 cancelText: $translate.instant('canceltextctrl'), 
		 okText: $translate.instant('oktextctrl') 
	   });

	   confirmPopup.then(function(res) 
	   {
		if(res) 
		{
			send_params = 
			{
				"appoinmenet_id" : id
			}
			
			//alert (id);

			  
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
			
			send_params = 
			{
				"appId" : id,
			}
			
			$http.post($rootScope.laravelHost+'DeleteAppointment',send_params)
			
			.success(function(data, status, headers, config)
			{
				$ionicPopup.alert({
					 title: $translate.instant('okdeletedctrl'),
					 template: '',
					 okText: $translate.instant('oktextctrl'),	
					});	
					$scope.getDataFun(0);
				//$scope.isBookedAppointment = 0;
			})
			.error(function(data, status, headers, config)
			{
			});	
			
						 
			
			  
			  //alert ($scope.NewUrl)
			
			/*$http.post($rootScope.Host+'/'+$scope.NewUrl,send_params)
			.success(function(data, status, headers, config)
			{
				$ionicPopup.alert({
				 title: $translate.instant('okdeletedctrl'),
				 template: ''
				});	
				
				$scope.appoinements.splice(index, 1);
				
			})
			.error(function(data, status, headers, config)
			{
			});	*/
		  } 
		});			
	}	
	
	
	
	$scope.Days = new Array();
	$scope.setDays = function ()
	{
		$scope.Days = new Array();
		
		for(i=0;i<14;i++)
		{
			$scope.Obj = new Object();
			$scope.hours = ((i-1)*24)+24;
			$scope.Day = new Date(new Date().getTime() + $scope.hours * 60 * 60 * 1000);
		//	console.log($scope.Day + " : " + $scope.Day.getDay())
			$scope.DD = $scope.gatDayName($scope.Day.getDay());
			//console.log($scope.gatDayName($scope.Day.getDay()) + " : " + $scope.Day.getDay())
			$scope.Day = $scope.Day.toISOString().substring(0, 10);
			$scope.splitDay = $scope.Day.split('-');
			$scope.Day = $scope.splitDay[2]+ '-' + $scope.splitDay[1] + '-' + $scope.splitDay[0];
			$scope.Obj.date = $scope.Day;
			$scope.Obj.day = $scope.DD;
			$scope.Days.push($scope.Obj)
		}
		
		$scope.CurrentDayText =  $scope.Days[0].day + " " + $scope.Days[0].date;
		$scope.CurrentDay = $scope.Days[0].date;
	
	
		$scope.Days = $scope.Days.reverse();
		setTimeout(function() {
			$ionicScrollDelegate.$getByHandle('daysScroll').scrollTo(-300,0,true);
			},200);
			
	//	console.log("Days : " ,$scope.Days)
	}
	
	$scope.setCurrentDay = function(day) 
	{
		$scope.daysSearch = true;
		$scope.showSearch = false;
		$scope.CurrentDayText = "יום " + $scope.Days[day].day + "  |  " + $scope.Days[day].date;
		$scope.CurrentDay = $scope.Days[day].date;  
		
		$scope.splitDay1 = $scope.CurrentDay.split('-');
		$scope.cDay = $scope.splitDay1[0]+ '/' + $scope.splitDay1[1] + '/' + $scope.splitDay1[2];
		
		$scope.getDataFun(0);
		console.log("cDay" , $scope.cDay)
	}
			

				
	$scope.gatDayName = function(day)
	{
		  var Day = '';
		  switch(day)
		  {
				case 6: Day = $translate.instant('sevendayctrl');	
				break; 
				case 0:Day = $translate.instant('onedayctrl');	
				break; 
				case 1:Day = $translate.instant('twodayctrl');	
				break; 
				case 2:Day = $translate.instant('threedayctrl');	
				break; 
				case 3:Day = $translate.instant('fourdayctrl');		
				break; 
				case 4:Day = $translate.instant('fivedayctrl');	
				break; 
				case 5:Day = $translate.instant('sixdayctrl');
				break; 
		   }
		   return Day;  
	 }


		
	$scope.myFilter = function (item) 
	{ 
		$scope.DaysArray = $scope.CurrentDay.split("-");
		$scope.CurrentDay1 = $scope.DaysArray[0] + "/"+ $scope.DaysArray[1] +"/"+ $scope.DaysArray[2];
		//console.log("Filter : " + item.start_date + " : "  + $scope.CurrentDay1 )
		if(String(item.start_date) == String($scope.CurrentDay1))
		{
    		return item;
		}
	};
	
	
	$scope.setDays();
	
	$scope.editFreeApp = function(index,id)
	{
		window.location.href = "#/app/editfreeapp/"+index;
	}
	
	
	$scope.editAppoint = function(index,id)
	{
		window.location.href = "#/app/editappoint/"+id;
	}
				
})

.controller('EditFreeAppCtrl', function($scope, $stateParams,loadMainJson,$rootScope,$ionicLoading,$state,$http,$localStorage,$ionicPopup,$sce,dateFilter,$timeout,$translate) 
{
	$scope.itemId = $stateParams.ItemId;
	$scope.AppType = 0;
	
	$scope.AppoinmentData = $rootScope.ClientAppointments[$scope.itemId];

	$scope.fields = 
	{
		"startime" : "",
		"endtime" : "",
		"date" : "",
		"appid":$scope.AppoinmentData.index
	}
	
	
	

	console.log("app: ", $scope.AppoinmentData)


	 $timeout(function() 
	 {

		$scope.fields.startime = $scope.AppoinmentData.start_time;
		$scope.fields.endtime = $scope.AppoinmentData.end_time;
		$scope.fields.date = $scope.AppoinmentData.start_date;
	 }, 300);

	$scope.selectTime = function(type)
	{
		var options = {
			date: new Date(),
			mode: 'time',
			androidTheme: 2,
			is24Hour : true,
			okText : $translate.instant('datepickoktext'),
			cancelText: $translate.instant('datepickcanceltext')			
		};
		

		
		datePicker.show(options, function(date){
		  //alert("date result " + date);  
			$scope.newHour = date.toString().split(" ")[4];
			$scope.splitHour = $scope.newHour.split(":");
			$scope.newTime = $scope.splitHour[0]+":"+$scope.splitHour[1];
			
			//
			if (type == 0)
			{
				$scope.fields.startime = $scope.newTime;
			}
			else
			{
				$scope.fields.endtime = $scope.newTime;
			}
			
			
			$scope.updateTime(type);


		
		
		});
			

			

		//datePicker.show(options, onSuccess, onError);		
	}


	$scope.updateTime = function(type)
	{
		 $timeout(function() 
		 {
			 
			if (type == 0)
			{
				$scope.fields.startime = $scope.newTime;
			}
			else
			{
				$scope.fields.endtime = $scope.newTime;
			}
			
			//alert ($scope.appoinmenet.hourstart)
			//alert ($scope.appoinmenet.hourend)
		}, 100);
	}

	
	
	$scope.updateFreeApp = function()
	{
			$ionicLoading.show({
			  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
			});			
		
			send_params = 
			{
				"supplierId" : $localStorage.supplierid,
				"userId" : "",
				"day" : $scope.fields.date,
				"start" : $scope.fields.startime,
				"end" : $scope.fields.endtime,  // "17:00" 
				"type" : $scope.AppType,
				"name" : "",
				"phone" : "",
				"mail" : "",
				"desc" : "",
				"subcat" : "",
				"send" : 1,
				"appid" : $scope.fields.appid
			}
			
			/*send_params = 
			{
				"appid" : $scope.fields.appid,
				"type" : 0
			}*/
		
			
			console.log("ADDParams : " , send_params)
		//	console.log(send_params);
			//return;
			
			
			$http.post($rootScope.laravelHost+'UpdateApointment',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				console.log("Response : " ,data )
				if(data == 1)
				window.location = "#/app/supplierappoint";
				else
				{
					$ionicPopup.alert({
						 title: $translate.instant('tortimeconflictctrl'),
						 template: '',
						okText: $translate.instant('oktextctrl'),	
					});	
				}
			})
			.error(function(data, status, headers, config)
			{
			});			
	}

	
	
	//alert ($scope.AppoinmentData)
	
	//alert ($scope.itemId)
})	


.controller('EditAppointCtrl', function($scope, $stateParams,loadMainJson,$rootScope,$ionicLoading,$state,$http,$localStorage,$ionicPopup,$sce,dateFilter,$timeout,$translate) 
{
	$scope.itemId = $stateParams.ItemId;
	$scope.AppType = 1;
	$scope.isBookedAppointment = 0;
	
	$scope.fields = 
	{
		"name" : "",
		"phone" : "",
		"mail" : "",
		"date" : "",
		"startime" : "",
		"endtime" : "",
		"desc" : "",
		"subcat" : "",
		"booked" : ""
	}
	




	$scope.changeType = function(type)
	{
		$scope.AppType = type;
	}
	

	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
	send_params = 
	{
		"userId" : $localStorage.supplierid
	}
	console.log($rootScope.laravelHost+"GetCategoriesByUser")
	$http.post($rootScope.laravelHost+'GetCategoriesByUser',send_params)
	.success(function(data, status, headers, config)
	{
		$scope.Catgeories = data;
		$scope.SubCat = data[0].sub;
		console.log("Catgeories : " , $scope.Catgeories )
	})
	.error(function(data, status, headers, config)
	{
	});	
	
			
	$scope.cat = 
	{
		"current":"",
		"subcat":""
	};	
	
	$scope.$watch('cat.current', function () 
	{   
		if($scope.Catgeories && $scope.cat.current != -1)
		{
			console.log("CT : " , $scope.Catgeories);
		//	place = parseInt($scope.cat.current);
			for(var i=0;i<$scope.Catgeories.length; i++)
			{
				console.log("CP " , i , $scope.Catgeories[i].catId , $scope.cat.current)
				if($scope.Catgeories[i].catId == $scope.cat.current)
				place = i;
			}
			
			$scope.SubCat = $scope.Catgeories[place].sub;
			
			console.log("subcat: ", $scope.Catgeories, ":", place,"--------")
			//console.log($scope.cat.current + " : " , $scope.SubCat )
			$scope.cat.subcat = $scope.SubCat[place].id;
			$scope.fields.subcat = $scope.cat.subcat;
		}
		else
		{
			//$scope.cat.subcat = -1;
			//$scope.fields.subcat = $scope.cat.subcat;
		}
	});
		
		
		
		
	
	$scope.getDetails = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		
		send_params = 
		{
			"id" : $scope.itemId,
			/*
			"name" : $scope.fields.name,
			"phone" : $scope.fields.phone,
			"mail" : $scope.fields.mail,
			"date" : $scope.fields.date,
			"startime" : $scope.fields.startime,
			"endtime" : $scope.fields.endtime,
			"desc" : $scope.fields.desc,
			*/
		}
		
		$http.post($rootScope.laravelHost+'ApointmentDetails',send_params)
		.success(function(data, status, headers, config)
		{
			
		 $timeout(function() 
		 {
			console.log("Appointment  ",  data[0])
			$scope.AppoinementData = data[0];
			$scope.fields.name = $scope.AppoinementData.name;
			$scope.fields.phone = $scope.AppoinementData.phone;
			$scope.fields.mail = $scope.AppoinementData.email;
			$scope.fields.date = $scope.AppoinementData.start_date;
			$scope.fields.startime = $scope.AppoinementData.start_time;
			$scope.fields.endtime = $scope.AppoinementData.end_time;
			$scope.fields.desc = $scope.AppoinementData.desc;
			$scope.fields.booked = $scope.AppoinementData.booked;
			$scope.cat.current = $scope.AppoinementData.cut;
			$scope.cat.subcat = $scope.AppoinementData.subcat;
			$scope.fields.appid = $scope.AppoinementData.index;
			if($scope.fields.booked == 1)
			$scope.isBookedAppointment = 1;
		
		
		}, 300);


		

		})
		.error(function(data, status, headers, config)
		{
		});	
		
	}
	
	$scope.getDetails();
	
	$scope.emptyAppoint = function()
	{
		
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
			
			send_params = 
			{
				"appId" : $scope.itemId,
			}
			
			$http.post($rootScope.laravelHost+'DeleteAppointment',send_params)
			
			.success(function(data, status, headers, config)
			{
				console.log("Delete : " , data , " : " ,$scope.itemId)
				//$scope.isBookedAppointment = 0;
			})
			.error(function(data, status, headers, config)
			{
			});	
		

			
	   var confirmPopup = $ionicPopup.confirm({
		 title: '<p dir="'+$rootScope.lanDirction+'">'+$translate.instant('torchangedfreectrl')+'</p>',		 
		 template: '',
		 cancelText: $translate.instant('backtorimctrl'),
		 okText: $translate.instant('newtorctrl')	 
	   });
		console.log("saveAppointment1")
	   confirmPopup.then(function(res) {
		if(res) 
		{
			$scope.isBookedAppointment = 0;
		} 
		 else 
		 {
			window.location = "#/app/supplierappoint";			 
			 
		 }
	   });				

		
	}
	
	
	$scope.updateAppoint = function()
	{
		if ($scope.cat.current =="-1" && $scope.AppType == 1)
		{
			$ionicPopup.alert({
			 title: $translate.instant('selectpickcatctrl'),
			 template: '',
			 okText: $translate.instant('oktextctrl'),	
			});	
		}
		else if ($scope.cat.subcat =="-1" && $scope.AppType == 1)
		{
			$ionicPopup.alert({
			 title: $translate.instant('selectsubcatctrl'),
			 template: '',
			 okText: $translate.instant('oktextctrl'),	
			});	
		}
		else
		{
			//alert (444);
			//return;
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
		
		
		send_params = 
		{
			"supplierId" : $localStorage.supplierid,
			"day" : $scope.fields.date,
			"subcat" : $scope.cat.subcat,
			"start" : $scope.fields.startime,
			"end" : $scope.fields.endtime,
			"type" : $scope.AppType,
			"name" : $scope.fields.name,
			"phone" : $scope.fields.phone,
			"mail" : $scope.fields.mail,
			"desc" : $scope.fields.desc,
			"send" : "1",
			"userId" : "1"

			/*
			"appointmentId" : $scope.itemId,
			"name" : $scope.fields.name,
			"phone" : $scope.fields.phone,
			"mail" : $scope.fields.mail,
			//"date" : $scope.fields.date,
			"start" : $scope.fields.startime,
			//"endtime" : $scope.fields.endtime,
			"desc" : $scope.fields.desc,
			"userId" : $localStorage.userid
			*/
		}
		console.log("params" ,send_params )
		$http.post($rootScope.laravelHost+'AddApointmentApp',send_params) //AddApointmentApp
		//$http.post($rootScope.laravelHost+'saveAppointment',send_params)
		.success(function(data, status, headers, config)
		{
			console.log("Save" , data);
			
			//window.location = "#/app/supplierappoint";	
			/*
			$ionicPopup.alert({
			 title: 'תור נשמר בהצלחה',
			 template: ''
			});	
			*/
		})
		.error(function(data, status, headers, config)
		{
		});			
					
	 }
		
		

	}
	
	
	
	$scope.updateFullAppoint = function()
	{
			if ($scope.cat.current =="-1" && $scope.AppType == 1)
			{
				$ionicPopup.alert({
				 title: $translate.instant('selectpickcatctrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),	
				});	
			}
			else if ($scope.cat.subcat =="-1" && $scope.AppType == 1)
			{
				$ionicPopup.alert({
				 title: $translate.instant('selectsubcatctrl'),
				 template: '',
				 okText: $translate.instant('oktextctrl'),	
				});	
			}
			else
			{
			
				$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
			
			
				send_params = 
				{
					"supplierId" : $localStorage.supplierid,
					"day" : $scope.fields.date,
					"subcat" : $scope.cat.subcat,
					"start" : $scope.fields.startime,
					"end" : $scope.fields.endtime,
					"type" : $scope.AppType,
					"name" : $scope.fields.name,
					"phone" : $scope.fields.phone,
					"mail" : $scope.fields.mail,
					"desc" : $scope.fields.desc,
					"send" : "1",
					"userId" : "1",
					"appid" : $scope.fields.appid
				}
				
			
				console.log("params" ,send_params )
				$http.post($rootScope.laravelHost+'UpdateApointment',send_params)
				//$http.post($rootScope.laravelHost+'saveAppointment',send_params)
				.success(function(data, status, headers, config)
				{
					console.log("Save" , data);
					
					window.location = "#/app/supplierappoint";	
					/*
					$ionicPopup.alert({
					 title: 'תור נשמר בהצלחה',
					 template: ''
					});	
					*/
				})
				.error(function(data, status, headers, config)
				{
				});						
		 }
	}
	
	
	
	$scope.selectTime = function(type)
	{
		var options = {
			date: new Date(),
			mode: 'time',
			androidTheme: 2,
			is24Hour : true,
			okText : $translate.instant('datepickoktext'),
			cancelText: $translate.instant('datepickcanceltext')			
		};
		

		
		datePicker.show(options, function(date)
		{
		  //alert("date result " + date);  
			$scope.newHour = date.toString().split(" ")[4];
			$scope.splitHour = $scope.newHour.split(":");
			$scope.newTime = $scope.splitHour[0]+":"+$scope.splitHour[1];
			if (type == 0)
				$scope.fields.startime = $scope.newTime;		
			else
				$scope.fields.endtime = $scope.newTime;
		});
		//datePicker.show(options, onSuccess, onError);		
	}			
})	



.controller('SupplierAddAppoinmenet', function($scope, $stateParams,loadMainJson,$rootScope,$ionicLoading,$state,$http,$localStorage,$ionicPopup,$sce,dateFilter,$timeout,$translate) 
{

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; 
	var yyyy = today.getFullYear();
	$scope.SubCat = Array();
	
	if(dd<10) {
		dd='0'+dd
	} 

	if(mm<10) {
		mm='0'+mm
	} 

	$scope.today = dd+'/'+mm+'/'+yyyy;
	$scope.todaynewdate = yyyy+'-'+mm+'-'+dd;
	

	
	


		
	$scope.appoinmenet = 
	{
		"date" : $scope.today,
		"hourstart" : $translate.instant('editappoint_starthour'), // "09:00",
		"hourend" : $translate.instant('editappoint_endhour'), // "10:00"
		"name" : "",
		"phone": "",
		"mail" : "",
		"desc" : ""
	}
	

	
	$scope.AppType = 0;
	
	$scope.changeType = function(type)
	{
		$scope.AppType = type;
	}
	
	
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8; application/json';
	send_params = 
	{
		"userId" : $localStorage.supplierid
	}
		//console.log($rootScope.laravelHost+"GetCategoriesByUser")
	$http.post($rootScope.laravelHost+'GetCategoriesByUser',send_params)
	.success(function(data, status, headers, config)
	{
		$scope.Catgeories = data;
		$scope.SubCat = data[0].sub;
		console.log("GetCategoriesByUser:" , data);
		//console.log($scope.SubCat )
	})
	.error(function(data, status, headers, config)
	{
	});	
	
			
	$scope.cat = 
	{
		"current":"-1",
		"subcat":"-1"
	};	
	
	$scope.$watch('cat.current', function () 
	{   
		if($scope.Catgeories && $scope.cat.current != -1)
		{
			console.log($scope.cat.current);
			//place = parseInt($scope.cat.current);
			for(var i=0;i<$scope.Catgeories.length; i++)
			{
				console.log("CP " , i , $scope.Catgeories[i].catId , $scope.cat.current)
				if($scope.Catgeories[i].catId == $scope.cat.current)
				place = i;
			}
			
			$scope.SubCat = $scope.Catgeories[place].sub;
			console.log($scope.cat.current + " : " , $scope.SubCat )
			$scope.cat.subcat = $scope.SubCat[place].id;
		}
		else
		{
			$scope.cat.subcat = -1;
		}
	});
		
	$scope.pickTime = function(type)
	{
		var options = {
			date: new Date(),
			mode: 'time',
			androidTheme: 2,
			is24Hour : true,
			okText : $translate.instant('datepickoktext'),
			cancelText: $translate.instant('datepickcanceltext')			
		};
		

		
		datePicker.show(options, function(date){
		  //alert("date result " + date);  
			$scope.newHour = date.toString().split(" ")[4];
			$scope.splitHour = $scope.newHour.split(":");
			$scope.newTime = $scope.splitHour[0]+":"+$scope.splitHour[1];
			
			//
			if (type == 1)
			{
				$scope.appoinmenet.hourstart = $scope.newTime;
			}
			else
			{
				$scope.appoinmenet.hourend = $scope.newTime;
			}
			
			
			$scope.updateTime(type);


		
		
		});
			

			

		//datePicker.show(options, onSuccess, onError);		
	}


	$scope.updateTime = function(type)
	{
		 $timeout(function() 
		 {
			 
			if (type == 1)
			{
				$scope.appoinmenet.hourstart = $scope.newTime;
			}
			else
			{
				$scope.appoinmenet.hourend = 	$scope.newTime;
			}
			
			//alert ($scope.appoinmenet.hourstart)
			//alert ($scope.appoinmenet.hourend)
		}, 100);
	}

	


	
	$scope.addAppoinment = function()
	{
		
		//alert ($scope.AppType)
		//return;
		
		if ($scope.AppType == "0")
		{
			$scope.AppDesc = $translate.instant('activitytimectrl');
			$scope.addOk = $translate.instant('pluraladdoktext');
		}
		else
		{
			$scope.AppDesc = $translate.instant('newtorctrl');
			$scope.addOk = $translate.instant('addoktextctrl');
		}
			
		$scope.successText = $scope.AppDesc+' '+$scope.addOk;
		

		$scope.reversedate =  $scope.appoinmenet.date.split("-").reverse().join("/");;
		$scope.splitdate = $scope.reversedate.split("/");
		$scope.formatteddate = $scope.splitdate[2]+'-'+$scope.splitdate[1]+'-'+$scope.splitdate[0];


		//alert ($scope.appoinmenet.date)
		
		//alert ($scope.appoinmenet.date);
		if ($scope.appoinmenet.date =="")
		{
			$ionicPopup.alert({
			 title: $translate.instant('inputtordatectrl'),
			 template: '',
			 okText: $translate.instant('oktextctrl'),	
			});				
		}
		else if ($scope.appoinmenet.hourstart == $translate.instant('editappoint_starthour'))
		{
			$ionicPopup.alert({
			 title: $translate.instant('inputstarttimectrl'),
			 template: '',
			 okText: $translate.instant('oktextctrl'),	
			});				
		}
		else if ($scope.appoinmenet.hourend == $translate.instant('editappoint_endhour')  && $scope.AppType == 0)
		{
			$ionicPopup.alert({
			 title: $translate.instant('inputendtimectrl'),
			 template: '',
			 okText: $translate.instant('oktextctrl'),	
			});				
		}
		
		else if ($scope.todaynewdate > $scope.formatteddate)
		{
			$ionicPopup.alert({
			 title: $translate.instant('cantaddctrl')+' '+$scope.AppDesc+' '+$translate.instant('previousdayctrl'),
			 template: '',
			okText: $translate.instant('oktextctrl'),	
			});	
		}
		else if ($scope.AppType == 0 && $scope.appoinmenet.hourstart > $scope.appoinmenet.hourend)
		{
			$ionicPopup.alert({
			 title: $translate.instant('starttimecantbelargectrl'),
			 template: ''
			});	
		}
		else if ($scope.AppType == 1 && $scope.cat.current == -1)
		{
			$ionicPopup.alert({
			 title: $translate.instant('selectpickcatctrl'),
			 template: '',
			 okText: $translate.instant('oktextctrl'),	
			});	
		}
		else if ($scope.AppType == 1 && $scope.cat.subcat == -1)
		{
			$ionicPopup.alert({
			 title: $translate.instant('selectsubcatctrl'),
			 template: '',
			 okText: $translate.instant('oktextctrl'),	
			});	
		}		
		else
		{
			

			$scope.newdate = dateFilter($scope.appoinmenet.date, 'dd/MM/yyyy');

			
			//$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
			/*$http.post($rootScope.Host+'/add_appoinement.php',send_params)
			send_params = 
			{
				"supplier_id" : $localStorage.supplierid,
				"date" : $scope.newdate,
				"start" : $scope.appoinmenet.hourstart,
				"end" : $scope.appoinmenet.hourend,
				"type" : $scope.AppType,
				"name" : $scope.appoinmenet.name,
				"phone" : $scope.appoinmenet.phone,
				"mail" : $scope.appoinmenet.mail,
				"desc" : $scope.appoinmenet.desc,
				"send" : 1
			}
			*/
			
			//alert($scope.cat.subcat)
			
			$ionicLoading.show({
			  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
			});		
			
			send_params = 
			{
				"supplierId" : $localStorage.supplierid,
				"userId" : "",
				"day" : $scope.newdate,
				"start" : $scope.appoinmenet.hourstart,
				"end" : $scope.appoinmenet.hourend,
				"type" : $scope.AppType,
				"name" : $scope.appoinmenet.name,
				"phone" : $scope.appoinmenet.phone,
				"mail" : $scope.appoinmenet.mail, // "1", 
				"desc" : $scope.appoinmenet.desc,
				"cat" : $scope.cat.current, //"47", 
				"subcat" : $scope.cat.subcat, //"47", 
				"send" : 1,
			}
			
			console.log("ADDParams : " , send_params)
		//	console.log(send_params);
			//return;
			
			
			$http.post($rootScope.laravelHost+'AddApointmentApp',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				console.log("Response : " ,data )
				
				if ($scope.AppType == 0)
				{
					if(data == 0)
					{

						$ionicPopup.alert({
						 title: $translate.instant('cantaddtortimedatectrl'),
						 template: '',
						 okText: $translate.instant('oktextctrl'),	
						});		
					}
					else
					{
						$ionicPopup.alert({
						 title: $scope.successText,
						 template: '',
						 okText: $translate.instant('oktextctrl'),	
						});	
						
						window.location = "#/app/supplierappoint";		
					}					
				}
				else
				{
					if(data == 0)
					{
						
						$ionicPopup.alert({
						 title: $translate.instant('cantaddtortimedatectrl1'),
						 template: '',
						 okText: $translate.instant('oktextctrl'),	
						});	
						
	
					}

					else if(data == 1)
					{
						$ionicPopup.alert({
						 title: $translate.instant('cantaddtortimedatectrl'),
						 template: '',
						okText: $translate.instant('oktextctrl'),	
						});	
	
					}
					else
					{
						$ionicPopup.alert({
						 title: $scope.successText,
						 template: '',
						 okText: $translate.instant('oktextctrl'),	
						});	
						
						window.location = "#/app/supplierappoint";		
					}					
				}
				
				

			})
			.error(function(data, status, headers, config)
			{
			});	
			
			
	
		}
	}
	
	
	
				
	
	
})	



.controller('CouponsCtrl', function($scope, $stateParams,loadMainJson,$rootScope,$ionicLoading,$state,$http,$localStorage,$ionicPopup,$sce,dateFilter,$timeout) 
{
	$timeout(function() {
		$scope.host = $rootScope.Host;
		$scope.Coupons= $rootScope.CouponsArray;
    }, 500);
	
	$scope.checkPercent = function(deal)
	{
		z=deal.old_price-deal.price;
		z= z/deal.old_price;
		z = parseInt(z*100);
		//alert(parseInt(deal.price)*parseInt(deal.discount_precent/10))
		return z;
	}	

	$scope.cutDate = function(date)
	{
		var dArray = date.split('/');
		dateStr = dArray[0]+"."+dArray[1]
		return dateStr;
	}
	
	$scope.cutYear = function(date)
	{
		var dArray = date.split('/');
		dateStr = dArray[2]
		return dateStr
	}	
	

})

.controller('CouponsDetailsCtrl', function($scope, $stateParams,loadMainJson,$rootScope,$ionicLoading,$state,$http,$localStorage,$ionicPopup,$sce,dateFilter,$timeout) 
{
	$timeout(function() {
		$scope.host = $rootScope.Host;		
		$scope.Deal = $rootScope.CouponsArray[$stateParams.ItemId];
		$scope.Gallery = [$scope.Deal.image,$scope.Deal.image2];
		$scope.host = $rootScope.Host;
		$scope.dealsImageGallery = $scope.Deal.gallery;
		
		console.log($scope.dealsImageGallery)
		
		$scope.SupplierName = function(SupplierIndex)
		{
			var sName = DealsFactory.getSupplierNameById(SupplierIndex);
			return sName;
		}		
		
		
		
		
    }, 100);
	

})

.controller('RatingsCtrl', function($scope, $stateParams,loadMainJson,$rootScope,$ionicLoading,$state,$http,$localStorage,$ionicPopup,$sce,dateFilter,$timeout,$translate) 
{
	$scope.SupplierId = $stateParams.ItemId;
	$scope.Empty = 'img/s2.png';
	$scope.Full = 'img/s1.png';
	$scope.RateArray = new Array();
	
	$scope.openQuestion = 
	{
		"openquestion" : ''
	}
	
	

	
	
	$scope.sendFeedback  = function() { 
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		$scope.Answers = '';
		$scope.NotAnswerQuestion = 0;
		
		for(i=0;i<$scope.RateArray.length;i++)
		{
			if($scope.RateArray[i]["val"] != 0)
			$scope.Answers += String($scope.RateArray[i]["val"]+',')
			else
			$scope.NotAnswerQuestion = i+1;
		}
		console.log($scope.Answers + " : " + $scope.openQuestion.openquestion)
		data = 
		{	
			"supplier_index" : $scope.SupplierId, 
			"answers" : $scope.Answers,
			"openquestion" : $scope.openQuestion.openquestion
		}
		
		$scope.Answers = ''
		
		if($scope.NotAnswerQuestion == 0)
		{
			$http.post($rootScope.Host+'/send_feedback.php',data)
			.success(function(data, status, headers, config)
			{
				$scope.openQuestion = '';
				
				$ionicPopup.alert({
				 title: $translate.instant('thankyouderugredirectctrl'),
				buttons: [{
					text: $translate.instant('oktextctrl'),
					type: 'button-positive',
				  }]
				});
   
			window.location.href = "#/app/main";
			})
			.error(function(data, status, headers, config)
			{
				//alert("f : " +data.responseText)
				//console.log('error : ' + data);
				//console.log('error : ' + data);
			});
		}
		else
		{
			
				$ionicPopup.alert({
				 title: $translate.instant('notratedquestionctrl')+" " + $scope.NotAnswerQuestion,
				buttons: [{
					text: $translate.instant('oktextctrl'),
					type: 'button-positive',
				  }]
				});
				
		}
	}
	
	$http.get($rootScope.Host+'/get_questions.php')
    .success(function(data, status, headers, config)
     {
			$scope.Questions = data;
			
			for(i=0;i<$scope.Questions.length;i++)
			{
				$scope.Rate = new Object();
				$scope.Rate =
				{
					"rate1" : $scope.Empty,
					"rate2" : $scope.Empty,
					"rate3" : $scope.Empty,
					"rate4" : $scope.Empty,
					"rate5" : $scope.Empty,
					"val" : 0
				}
	
				$scope.RateArray[i] = $scope.Rate;
			}
			
			console.log($scope.RateArray)
     })
     .error(function(data, status, headers, config)
     {
     });
	 
	
	 
	 $scope.starClick = function (nm1,nm2)
	 {
		 for(i=1;i<=nm1;i++)
		 {
			 console.log("Full : " + i)
			 $scope.RateArray[nm2]["rate"+i] = $scope.Full;
		 }
		 
		 for(i=5; i>nm1;i--)
		 {
			console.log("Empty : " + i)
			$scope.RateArray[nm2]["rate"+i] = $scope.Empty; 
		 }
		 
		 $scope.RateArray[nm2]["val"] = nm1;
		 
		 console.log($scope.RateArray)
		 console.log($scope.RateArray[nm2]["val"] + " : " + $scope.nm1)
	 }

	 
})

.controller('LoginRegisterCtrl', function($scope,$stateParams, $ionicModal, $timeout,$localStorage,$ionicPopup,$http,$ionicLoading,$q,$rootScope,$translate) {


	$scope.ItemId = $stateParams.ItemId;
	$scope.pushId = '';

	$scope.login = 
	{
		"name" : "",
		"phone" : "",
		"area" : "1",
		"email" : "",
		"password" : "",
		"terms" : false,
		
	};
	
	
		
		

		$scope.agreeTerms = function(type)
		{
			$scope.login.terms = type;
		}
		
		$scope.openTerms = function()
		{
			//cordova.InAppBrowser.open('http://apache.org', '_system', 'location=yes');
			//window.open("http://tapper.co.il/mami/terms.pdf", "_blank");
			if ($rootScope.DefaultLanguage == "he")
				iabRef = window.open("http://tapper.co.il/mami/terms.pdf", '_system', 'location=yes');
			else
				iabRef = window.open("http://tapper.co.il/mami/terms_english.pdf", '_system', 'location=yes');
			
		}
	
  // This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
    .then(function(profileInfo) {


	$scope.FacebookLoginFunction(profileInfo.id,profileInfo.first_name,profileInfo.last_name,profileInfo.email);

	
      $ionicLoading.hide();
      //$state.go('app.home');
    }, function(fail){
      // Fail get profile info
      console.log('profile info fail', fail);
    });
  };

  // This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError', error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name,first_name,last_name,locale&access_token=' + authResponse.accessToken, null,
      function (response) {
				console.log(response);
        info.resolve(response);
      },
      function (response) {
				console.log(response);
        info.reject(response);
      }
    );
    return info.promise;
  };

  //This method is executed when the user press the "Login with facebook" button
  $scope.FaceBookLoginBtn = function(type) {
	  
	  $scope.LoginType = type;
	  
	  
    facebookConnectPlugin.getLoginStatus(function(success){
      if(success.status === 'connected'){
        // The user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        console.log('getLoginStatus', success.status);


					getFacebookProfileInfo(success.authResponse)
					.then(function(profileInfo) {

							
						$scope.FacebookLoginFunction(profileInfo.id,profileInfo.first_name,profileInfo.last_name,profileInfo.email);


						//$state.go('app.home');
					}, function(fail){
						// Fail get profile info
						//alert('profile info fail', fail);
					});
				
      } else {

 
				$ionicLoading.show({
				  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
				});		
 


        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });
  };
  



  
  $scope.FacebookLoginFunction = function(id,firstname,lastname,email)
  {
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
		
		$scope.fullname = firstname+' '+lastname;
			
			facebook_data = 
			{
				"id" : id,
				"firstname" : firstname,
				"lastname" : lastname,
				"email" : email,
				"fullname" : $scope.fullname,
				"pushid" : $rootScope.pushId
			}					

			$http.post($rootScope.Host+'/facebook_connect.php',facebook_data).success(function(data)
			{
				if (data.response.userid)
				{
					$localStorage.userid = data.response.userid;
					$localStorage.name = $scope.fullname;
					$localStorage.email = email;


					
				  if ($scope.ItemId == 0)
				  {
					  $scope.myOrders();
				  }
				  else if ($scope.ItemId == 1)
				  {
					  $scope.CouponsNav();
				  }		
				  else if ($scope.ItemId == 2)
				  {
					  window.history.back();
					 // $scope.openPage("comments");
					  $scope.CommentModal();
				  }					  
				  else if ($scope.ItemId == 3)
				  {
					  window.history.back();
					  $scope.RateSupplier();
				  }					  
				  else if ($scope.ItemId == 4)
				  {
					  window.history.back();
					  $scope.OrderAppoint();
				  }	
				  else if ($scope.ItemId == 5)
				  {
					  window.history.back();
					  $scope.SearchAppoint();
				  }	

				  
				}	
			});
  }
	

	
	$scope.loginBtn = function()
	{
		
		if ($scope.login.email == "")
		{
			$ionicPopup.alert({
			title: $translate.instant('fillinemailctrl'),
			template: '',
			okText: $translate.instant('oktextctrl'),	
			});				
		}
		else if ($scope.login.password =="")
		{
			$ionicPopup.alert({
			title: $translate.instant('fillinpasswordctrl'),
			template: '',
			okText: $translate.instant('oktextctrl'),	
			});				
		}
		else
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			$ionicLoading.show({
			  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
			});	
		
			send_params = 
			{
				"email" : $scope.login.email,
				"password" : $scope.login.password,
				"pushid" : $rootScope.pushId,
			}

			$http.post($rootScope.Host+'/login.php',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
//				console.log(data);
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					title: $translate.instant('wrongdetailsctrl'),
					template: '',
					okText: $translate.instant('oktextctrl'),	
					});					
				}
				else
				{
					$localStorage.userid = data.response.id;
					$localStorage.name = data.response.name;
					$localStorage.phone = data.response.phone;
					$localStorage.email = data.response.email;
					$localStorage.password = $scope.login.password;
				 // $scope.loginModal.hide();

				  $scope.login.email = '';
				  $scope.login.password = '';
				  
				  if ($scope.ItemId == 0)
				  {
					  $scope.myOrders();
				  }
				  else if ($scope.ItemId == 1)
				  {
					  $scope.CouponsNav();
				  }		
				  else if ($scope.ItemId == 2)
				  {
					  window.history.back();
					 // $scope.openPage("comments");
					  $scope.CommentModal();
				  }					  
				  else if ($scope.ItemId == 3)
				  {
					  window.history.back();
					  $scope.RateSupplier();
				  }					  
				  else if ($scope.ItemId == 4)
				  {
					  window.history.back();
					  $scope.OrderAppoint();
				  }	
				  else if ($scope.ItemId == 5)
				  {
					  window.history.back();
					  $scope.SearchAppoint();
				  }					  
				  
				}
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});				
		}
	}


	
	$scope.registerBtn = function()
	{
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		

	
		if ($scope.login.name =="")
		{
			$ionicPopup.alert({
			title: $translate.instant('fillinfullnamectrl'),
			template: '',
			okText: $translate.instant('oktextctrl'),	
			});				
		}
		else if ($scope.login.phone =="")
		{
			$ionicPopup.alert({
			title: $translate.instant('fillinphonectrl'),
			template: '',
			okText: $translate.instant('oktextctrl'),	
			});				
		}		
		else if ($scope.login.email =="")
		{
			$ionicPopup.alert({
			title: $translate.instant('fillinemailctrl'),
			template: '',
			okText: $translate.instant('oktextctrl'),	
			});				
		}
		else if ($scope.login.password =="")
		{
			$ionicPopup.alert({
			title: $translate.instant('fillinpasswordctrl'),
			template: '',
			okText: $translate.instant('oktextctrl'),	
			});				
		}
		else if ($scope.login.terms == false)
		{
			$ionicPopup.alert({
			title: $translate.instant('agreetermsctrl'),
			template: '',
			okText: $translate.instant('oktextctrl'),	
			});	
		}
		else
		{
			
			$ionicLoading.show({
			  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
			});	
		
			send_params = 
			{
				"name" : $scope.login.name,
				"phone" : $scope.login.phone,
				"area" : $scope.login.area,
				"email" : $scope.login.email,
				"password" : $scope.login.password,
				"pushid" : $rootScope.pushId
			}

			$http.post($rootScope.Host+'/register.php',send_params)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				console.log(data);
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					title: $translate.instant('emailinusectrl'),
					template: '',
					okText: $translate.instant('oktextctrl'),	
					});					
				}
				else
				{
					$localStorage.userid = data.response.userid;
					$localStorage.name = data.response.name;
					$localStorage.phone = data.response.phone;
					$localStorage.email = data.response.email;
					$localStorage.password = $scope.login.password;
					
				  //$scope.registerModal.hide();
				  $scope.login.name = '';
				  $scope.login.phone = '';
				  $scope.login.email = '';
				  $scope.login.password = '';

				  
					$ionicPopup.alert({
					 title: $translate.instant('welcomeinctrl')+' '+$localStorage.name,
					 template: '',
					okText: $translate.instant('oktextctrl'),	
				   });	

						   
				  if ($scope.ItemId == 0)
				  {
					  $scope.myOrders();
				  }
				  else if ($scope.ItemId == 1)
				  {
					  $scope.CouponsNav();
				  }		
				  else if ($scope.ItemId == 2)
				  {
					  window.history.back();
					 // $scope.openPage("comments");
					  $scope.CommentModal();
				  }					  
				  else if ($scope.ItemId == 3)
				  {
					  window.history.back();
					  $scope.RateSupplier();
				  }					  
				  else if ($scope.ItemId == 4)
				  {
					  window.history.back();
					  $scope.OrderAppoint();
				  }	
				  else if ($scope.ItemId == 5)
				  {
					  window.history.back();
					  $scope.SearchAppoint();
				  }	
				  
				  
				}
			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});			
		}
	
		
	}	
	
	
	

})


.controller('ForgotLoginCtrl', function($scope,$stateParams, $ionicModal, $timeout,$localStorage,$ionicPopup,$http,$ionicLoading,$q,$rootScope,$translate) {

	$scope.forgot = 
	{
		"email" : ""
	}
	
	$scope.sendUserPass = function()
	{
		if ($scope.forgot.email =="")
		{
			$ionicPopup.alert({
			title: $translate.instant('inputemailctrl'),
			template: '',
			okText: $translate.instant('oktextctrl'),	
			});			
		}
		else
		{
			
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			
			$ionicLoading.show({
			  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
			});	

			
			$http.post($rootScope.Host+'/forgot_login.php',$scope.forgot)
			.success(function(data, status, headers, config)
			{
				$ionicLoading.hide();
				
				if (data.response.status == 0)
				{
					$ionicPopup.alert({
					title: $translate.instant('usernotfoundtryagainctrl'),
					template: '',
					okText: $translate.instant('oktextctrl'),	
					});			

					$scope.forgot.email = '';	
				}
				else
				{
					$ionicPopup.alert({
					title: $translate.instant('formsentctrl'),
					template: '',
					okText: $translate.instant('oktextctrl'),	
					});	
					
					$scope.forgot.email = '';
					window.history.back();						
				}
				
				
			

			})
			.error(function(data, status, headers, config)
			{
				$ionicLoading.hide();
			});				
		}
		
	}


})

.controller('UpdateInfoCtrl', function($scope,$stateParams, $ionicModal, $timeout,$localStorage,$ionicPopup,$http,$ionicLoading,$q,$rootScope,$translate) {

	$scope.updateinfo = 
	{
		"name" : $localStorage.name,
		"phone" : $localStorage.phone,
		"email" : $localStorage.email,
		"password" : $localStorage.password
	}
	
	$scope.facebookId = $localStorage.fbid;
	
	if ($scope.facebookId)
		$scope.showPassword = false;
	else
		$scope.showPassword = true;

	

	

	$scope.SendDetails = function()
	{
		if ($scope.updateinfo.name =="")
		{
			$ionicPopup.alert({
			title: $translate.instant('fillinfullnamectrl'),
			template: '',
			okText: $translate.instant('oktextctrl'),	
			});				
		}
		else if ($scope.updateinfo.phone =="")
		{
			$ionicPopup.alert({
			title: $translate.instant('fillinphonectrl'),
			template: '',
			okText: $translate.instant('oktextctrl'),	
			});				
		}		
		else if ($scope.updateinfo.password =="" && $scope.facebookId =="")
		{
			$ionicPopup.alert({
			title: $translate.instant('fillinpasswordctrl'),
			template: '',
			okText: $translate.instant('oktextctrl'),	
			});				
		}
		else
		{
			
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


		
				$ionicLoading.show({
				  template: $translate.instant('loadingtextctrl')+'...<ion-spinner icon="spiral"></ion-spinner>'
				});		
		

				send_params = 
				{
					"user" : $localStorage.userid,
					"name" : $scope.updateinfo.name,
					"phone" : $scope.updateinfo.phone,
					"password" : $scope.updateinfo.password,
					//"send" : 1
				}

				
				$http.post($rootScope.Host+'/update_personal.php',send_params)
				.success(function(data, status, headers, config)
				{
					$ionicLoading.hide();
					
					$ionicPopup.alert({
					title: $translate.instant('updatedokctrl'),
					template: '',
					okText: $translate.instant('oktextctrl'),	
					});		

					
					$localStorage.name = $scope.updateinfo.name;
					$localStorage.phone = $scope.updateinfo.phone;
					
					if ($scope.updateinfo.password)
						$localStorage.password = $scope.updateinfo.password;


					
					window.location = "#/app/main";
					
				})
				.error(function(data, status, headers, config)
				{
					$ionicLoading.hide();
				});		

				


		}
	}

})


.filter('range', function () {
    return function (input, total) 
	{
    total = parseInt(total);

    for (var i=0; i<total; i++) {
      input.push(i);
    }
	
	
        return input;
    };
})

.filter('toTrusted', function ($sce) 
{
    return function (value) {
        return $sce.trustAsHtml(value);
    };
})


.filter('stripslashes', function () {
    return function (value) 
	{
	    return (value + '')
		.replace(/\\(.?)/g, function(s, n1) {
		  switch (n1) {
			case '\\':
			  return '\\';
			case '0':
			  return '\u0000';
			case '':
			  return '';
			default:
			  return n1;
		  }
		});
    };
})

.filter('dayFilter', function(){
    return function(value){
        console.log("Vl : " , value)
		return true;
    }
});
